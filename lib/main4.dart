import 'package:fip4/main2.dart';

class Person {
  String name;
  double age;
  double height;
  double weight;
  String ssn;

  Person(this.name, this.age, this.height, this.weight, this.ssn);

  void printData({String? x,bool? y,double? t}) {
    print(
        "Name : ${name} , Age ${age}, height ${height}, weight ${weight}, ssn ${ssn}");
  }

  printSpecificData([bool isPrintShown=false]) { // optional named parameter
    print("Hello");
  }
}

class Student extends Person {
  String studentId;
  List<double> marks;
  List<String> classes;

  Student(this.studentId, this.marks, this.classes, String name, double age,
      double height, double weight, String ssn)
      : super(name, age, height, weight, ssn);
//
}

class Employee extends Person {
  // String name;
  // double age;
  // double height;
  // double weight;
  // String ssn;
  String employeeId;
  double salary;
  int numOfAnnualVacations;

  Employee(String name, double age, double height, double weight, String ssn,
      this.employeeId, this.salary, this.numOfAnnualVacations)
      : super(name, age, height, weight, ssn);

  @override // annotation
  void printData({String? x,bool? y,double? t}) {
    // overriden method
    super.printData();
    print(
        "EmplyeeId ${employeeId}, Salary ${salary}, NumOfAnnualVacations ${numOfAnnualVacations}");
  }
  // void printData(String x) { // overload
  //   print(
  //       "Name : ${name} , Age ${age}, height ${height}, weight ${weight}, ssn ${ssn}");
  // }
  // void printSpecificData(bool isHelloPrinted){
  //    print("Hello");
  //  }
  void calcNewSalary() {
    salary = salary + (numOfAnnualVacations * 5 / 7);
  }
}

void main() {
  Employee hasan = Employee("Hasan", 18, 160, 60, "9921456789", "STEPBYSTEP51",
      1000, 14); // new object
  print(hasan.salary);
  print(hasan.ssn);
  hasan.ssn = "15615";
  print(hasan.ssn);
  hasan.printData();
  hasan.calcNewSalary();
  hasan.printData();
//
  Student tala = Student(
    "STEPBYSTEP",
    [],
    [],
    "Tala",
    18,
    160,
    60,
    "9921456789",
  );
  tala.printData(x: "",t: 5,y: false);
  hasan.printSpecificData();
}
