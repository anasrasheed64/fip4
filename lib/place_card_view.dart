import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'models/place_model.dart';

class PlaceCardView extends StatelessWidget {
  final PlaceModel model;

  const PlaceCardView({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 170,
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      width: MediaQuery.of(context).size.width,
      child: Card(
          color: Color(0xfff3f3f3),
          elevation: 8,
          shadowColor: Color(0xfff3f3f3),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(4),
                  bottomLeft: Radius.circular(4))),
          child: Row(
            children: [
              SizedBox(
                height: 170,
                width: 180,
                child: model.base64Img != null
                    ? Image.memory(
                        base64Decode(model.base64Img!),
                        fit: BoxFit.fill,
                      )
                    : CachedNetworkImage(
                        imageUrl: model.urlImage,
                        imageBuilder: (context, imageProvider) => Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        placeholder: (context, url) => SizedBox(
                          child: CircularProgressIndicator(
                            color: Colors.red,
                          ),
                          height: 30,
                          width: 30,
                        ),
                        errorWidget: (context, url, error) => Text("image is not found"),
                      ),
              ),
              SizedBox(
                width: 20,
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Text(model.name),
                      SizedBox(
                        height: 10,
                      ),
                      Text(model.location),
                      SizedBox(
                        height: 10,
                      ),
                      Text(model.desc),
                    ],
                  ),
                ),
              )
            ],
          )),
    );
  }
}
