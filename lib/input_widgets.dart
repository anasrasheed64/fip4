import 'package:custom_searchable_dropdown/custom_searchable_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class InputWidgets extends StatefulWidget {
  const InputWidgets({Key? key}) : super(key: key);

  @override
  State<InputWidgets> createState() => _InputWidgetsState();
}

class _InputWidgetsState extends State<InputWidgets> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  String? _errorText;
  final FocusNode _userNameFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();
  bool _isObscure = true;
  final formGlobalKey = GlobalKey<FormState>();
  List<String> _teachers = [
    "Mohammad",
    "Tala",
    "Mohannad",
    "Reem",
    "Saeed",
    "Kamal"
  ];
  String? _selectedTeacher;
  @override
  void initState() {
    // _usernameController.text = "Anas";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Inputs Widgets"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              height: 20,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.1),
              child: Center(
                  child: TextField(
                controller: _usernameController,
                enabled: true,
                decoration: _inputDecoration(),
                style: const TextStyle(color: Colors.red),
                maxLines: 1,
                textAlign: TextAlign.start,
                cursorColor: Colors.pink,
                cursorWidth: 2,
                cursorHeight: 20,
                showCursor: true,
                focusNode: _userNameFocusNode,
                maxLength: 30,
                buildCounter: (BuildContext context,
                        {int? currentLength, int? maxLength, bool? isFocused}) =>
                    Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Icon(Icons.accessibility),
                    Text(
                      "$currentLength",
                      style: const TextStyle(color: Colors.green),
                    ),
                  ],
                ),
                keyboardType: TextInputType.name,
                textInputAction: TextInputAction.next,
                onTap: () {
                  print("hello");
                },
                onChanged: (v) {
                  //validation
                  // print(v);
                },
                onSubmitted: (v) {
                  _passwordFocusNode.requestFocus();
                },
                textDirection: TextDirection.ltr,
                readOnly: false,
                enableSuggestions: true,
                autocorrect: true,
                textCapitalization: TextCapitalization.sentences,
                inputFormatters: [
                  // FilteringTextInputFormatter.deny(RegExp(r'\d')),
                  // NoSpaceFormatter(),
                ],
              )),
            ),
            Form(
              key: formGlobalKey,
              child: Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.symmetric(
                    horizontal: MediaQuery.of(context).size.width * 0.1),
                child: Center(
                    child: TextFormField(
                  controller: _passwordController,
                  enabled: true,
                  autovalidateMode: AutovalidateMode.always,
                  validator: (v) {
                    if (v == null || v.isEmpty) {
                      return "This field is mandetory";
                    } else if (v.length < 5) {
                      return "Password must be greater than 4 chars";
                    } else if (!RegExp(
                            r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$')
                        .hasMatch(v)) {
                      return "passsord not match the regex";
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                      border: _getBorder(),
                      errorBorder: _getBorder(),
                      focusedErrorBorder: _getBorder(),
                      disabledBorder: _getBorder(),
                      focusedBorder: _getBorder(),
                      enabledBorder: _getBorder(),
                      labelText: "Password",
                      prefixIcon: const Icon(
                        Icons.lock,
                        color: Colors.black,
                      ),
                      suffixIcon: IconButton(
                        icon: const Icon(Icons.remove_red_eye_outlined),
                        onPressed: () {
                          setState(() {
                            _isObscure = !_isObscure;
                          });
                        },
                      )),
                  obscureText: _isObscure,
                  style: const TextStyle(color: Colors.red),
                  maxLines: 1,
                  textAlign: TextAlign.start,
                  cursorColor: Colors.pink,
                  cursorWidth: 2,
                  cursorHeight: 20,
                  showCursor: true,
                  focusNode: _passwordFocusNode,
                  maxLength: 30,
                  buildCounter: (BuildContext context,
                          {int? currentLength,
                          int? maxLength,
                          bool? isFocused}) =>
                      Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Icon(Icons.accessibility),
                      Text(
                        "$currentLength",
                        style: const TextStyle(color: Colors.green),
                      ),
                    ],
                  ),
                  keyboardType: TextInputType.name,
                  textInputAction: TextInputAction.done,
                  onTap: () {
                    print("hello");
                  },
                  onChanged: (v) {
                    //validation
                    // print(v);
                  },
                  onFieldSubmitted: (v) {
                    // print(v + "onFieldSubmitted");
                  },
                  textDirection: TextDirection.ltr,
                  readOnly: false,
                  enableSuggestions: true,
                  autocorrect: true,
                  textCapitalization: TextCapitalization.sentences,
                  inputFormatters: [
                    // FilteringTextInputFormatter.deny(RegExp(r'\d')),
                    // NoSpaceFormatter(),
                  ],
                )),
              ),
            ),
            CustomSearchableDropDown(
              items: _teachers,
              label: 'Select Teacher',
              menuMode: true,
              menuHeight: 300,
              hideSearch: true,
              decoration: BoxDecoration(border: Border.all(color: Colors.blue)),
              prefixIcon: Padding(
                padding: const EdgeInsets.all(0.0),
                child: Icon(Icons.search),
              ),
              dropDownMenuItems: _teachers.map((item) {
                    return item;
                  }).toList() ??
                  [],
              onChanged: (value) {
                setState(() {
                  _selectedTeacher=value;
                });
                // if (value != null) {
                //   selected = value['class'].toString();
                // } else {
                //   selected = null;
                // }
              },
            ),
            const SizedBox(
              height: 10,
            ),
            ElevatedButton(
                onPressed: () {
                  print(_selectedTeacher);
                  showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          title: Text("This is title"),
                          backgroundColor: Colors.white,
                          elevation: 4,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                          content: Column(
                            children: [
                              Text("this is content"),
                              Text("this is content"),
                              Text("this is content"),
                              Text("this is content")
                            ],
                            mainAxisSize: MainAxisSize.min,
                          ),
                          actions: [
                            ElevatedButton(
                                onPressed: () {
                                  Navigator.of(context, rootNavigator: true)
                                      .pop();
                                },
                                child: Text("Close")),
                            ElevatedButton(
                                onPressed: () {
                                  Navigator.of(context, rootNavigator: true)
                                      .pop();
                                },
                                child: Text("Submit"))
                          ],
                          icon: Icon(Icons.add),
                          iconColor: Colors.green,
                          scrollable: false,
                          actionsAlignment: MainAxisAlignment.start,
                          // contentPadding: EdgeInsets.zero,
                          // titlePadding: EdgeInsets.zero,
                        );
                      });
                  // validation
                  // if (_usernameController.text.isEmpty) {
                  //   setState(() {
                  //     _errorText = "This field is mandetory";
                  //     _userNameFocusNode.requestFocus();
                  //   });
                  //   return;
                  // }
                  // if (_usernameController.text.length < 5) {
                  //   setState(() {
                  //     _errorText = "Username must be greater than 4 chars";
                  //     _userNameFocusNode.requestFocus();
                  //   });
                  //   return;
                  // }
                  // if (!RegExp(
                  //         r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$')
                  //     .hasMatch(_usernameController.text)) {
                  //   setState(() {
                  //     _errorText =
                  //         "please insert 1 letter 1 deigit,1Captial letter";
                  //     _userNameFocusNode.requestFocus();
                  //   });
                  //   return;
                  // }
                  // setState(() {
                  //   _errorText = null;
                  //   _userNameFocusNode.unfocus();
                  // });
                  // if (formGlobalKey.currentState!.validate()) {
                  //   print(_passwordController.text);
                  // }
                },
                child: const Text("submit"))
          ],
        ),
      ),
    );
  }

  InputDecoration _inputDecoration(
      {String title = "Username", bool isObscure = false}) {
    return InputDecoration(
      border: _getBorder(),
      enabledBorder: _getBorder(),
      focusedBorder: _buildOutlineInputBorder(),
      disabledBorder: _getBorder(),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: const BorderSide(color: Colors.brown, width: 2)),
      filled: true,
      fillColor: Colors.blue.shade100,
      // ctrl + alt + m
      // labelText: "Username",
      // labelStyle: TextStyle(
      //     color: Colors.black,
      //     fontWeight: FontWeight.bold,
      //     fontSize: 15)
      hintText: title,
      hintStyle: const TextStyle(color: Colors.red),
      // contentPadding: EdgeInsets.symmetric(horizontal: 40)
      prefixIcon: const Icon(Icons.person),
      suffixIcon: const Icon(
        Icons.remove_red_eye,
        color: Colors.black,
      ),
      isDense: true,
      errorText: _errorText,
      errorStyle:
          TextStyle(color: Colors.red.shade900, fontWeight: FontWeight.bold),
      errorMaxLines: 2,
      // prefixText: "962",
      // suffixText: "@Carefour",
      focusedErrorBorder: _buildOutlineInputBorder(),
    );
  }

  OutlineInputBorder _buildOutlineInputBorder() {
    return OutlineInputBorder(
        borderRadius: BorderRadius.circular(2),
        borderSide: const BorderSide(color: Colors.yellow, width: 3));
  }

  OutlineInputBorder _getBorder() {
    return OutlineInputBorder(
        borderRadius: BorderRadius.circular(8),
        borderSide: const BorderSide(color: Colors.green, width: 2));
  }
}

class NoSpaceFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.contains(' ')) {
      // Do not allow spaces
      return oldValue;
    }
    return newValue;
  }
}
// navigations
// TextButton
// bottom sheet
// transform
// Future Builder
// Stream Builder
// tabs
// bottom navigation bar
// drawer
 // 70-90 design
 // task 2 1
// hardware camera bluetooth gps permissions
// ux firebase database