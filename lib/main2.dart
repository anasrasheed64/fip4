// import 'package:flutter/material.dart';

void main() {
  // create new object
  String a = "Ahmad"; // new Object  HEAP
  //
  Person ahmad = Person("Ahmad", 22.5, 165, 85); // new object person HEAP
  ahmad._name = "Ahmad";
  ahmad._age = 22;
  ahmad._height = 170;
  ahmad._weight = 80;
  print("name " + ahmad._name);
  print("Age " + ahmad._age.toString());
  print("height " + ahmad._height.toString());
  print("weight " + ahmad._weight.toString());

  // ahmad.age=-5;
  // ahmad.height=-50;
  // ahmad.weight=-50;
  // ahmad._dateOfBirth="sdfsdf";
  // access modifier public private

  // Person person=Person();
  // person.height=200;
  // person.name="Anas";
  // person.weight=120;
  // String exception
  // double int null

  // print("name " + person.name);
  // print("Age " + person.age.toString());
  // print("height " + person.height.toString());
  // print("weight " + person.weight.toString());

  // Animal cat=Animal();
  // cat.food="Mouse";
  // cat.numOfLegs=4;
}

// object
// name age height weight ssn uniDegree ....

class Person {
  // encapsulation
  String _name; // global variable public
  double _age;
  double _height;
  double _weight;
  String? _ssn;
  String? _dateOfBirth; // private variable
  Person(this._name, this._age, this._height, this._weight); // official

  // constructor method init value object
  // Person(String n, double age, double height, double w) {
  //   // local
  //   this.name = n; // global variable = local
  //   this.age = age; // local = local
  //   this.height = height;
  //   weight = w;
  // }

  void printData(bool showName,
      {bool showAge = false, bool showHeight = true,bool? showWeight}) {
    // method
    if (showName) {
      print('Hello ' + _name);
    }
    if (showAge) {
      print('youe age is ' + _age.toString());
    }
    if (showHeight) {
      print('youe height is ' + _height.toString());
    }
    print('youe weight is ' + _weight.toString());
  }

  // void printDataWithAge() {
  //   // method
  //   print('Hello ' + _name);
  //   print('youe age is ' + _age.toString());
  //   print('youe height is ' + _height.toString());
  //   print('youe weight is ' + _weight.toString());
  // }

  void setHeight(double newHeight) {
    if (newHeight.isNegative) {
      print('height can not be negative');
      return;
    }
    _height = newHeight;
  }

  double getHeight() {
    return _height;
  }

  void setAge(double newAge) {
    // setter
    if (newAge.isNegative) {
      return;
    }
    this._age = newAge;
  } // method

  double getAge() {
    return _age;
  }

  double get height {
    return _height;
  }

  set height(double newHeight) {
    if (newHeight.isNegative) {
      print('height can not be negative');
      return;
    }
    _height = newHeight;
  }

  double get age {
    //=>_age;
    return _age;
  } // lambda expression

  set age(double newAge) {
    if (newAge.isNegative) {
      print('Age cannot be negative');
      return;
    }
    // method setter age
    _age = newAge;
  }

// setter getter optional parameters required parameters Object Inheritance
}

class Animal {
  String? name;
  double? age;
  double? height;
  double? weight;
  String? food;
  int? numOfLegs;
}
class Student {
  String? name;
  double? age;
  double? height;
  double? weight;
  String? ssn;
  String? studentId;
  List<double>? marks;
  List<String>? classes;
  //
}

class Employee {
  String? name;
  double? age;
  double? height;
  double? weight;
  String? ssn;
  String? employeeId;
  double? salary;
  int? numOfAnnualVacations;
}