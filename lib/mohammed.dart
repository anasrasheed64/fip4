void main() {
Mohammed mohammed = Mohammed(name: 'Mohammed', age: 23, value: 97);
print(mohammed.toString());
}

class Mohammed<T> {
  late String name;
  late int age;
  late T value;

  Mohammed({required this.name, required this.age, required this.value});

  @override
  String toString() {
    return 'Name : $name, Age : $age, value : $value';
  }
}