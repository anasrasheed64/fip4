void main (){
  Mohanad newObject = Mohanad("Mohanad", 23, 30215);

  newObject.toString();
}

class  Mohanad <T> {
  String name;
  int age;
  T value;
  Mohanad(this.name,this.age,this.value);

  @override
  String toString() {

    return "Your name is $name and Your Age is $age and the Value For you is $value";
  }
}