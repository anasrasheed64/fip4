import 'package:fip4/main2.dart';

void main() {
  Person person = Person("anas", 30, 180, 90);
  person.setAge(31);
  print(person.getAge()); // 31
  person.setAge(-10);
  print(person.getAge()); // 31
  person.age = -5; // public
  print(person.age);
  person.age = 25; // public
  print(person.age);
  
  //

  person.setHeight(182);
  print(person.getHeight());
  person.setHeight(-50);
  print(person.getHeight()); // 182
  person.height=-50;
  print(person.height);
  person.printData(false,showAge: true);
  print('**********');
  person.printData(true,showAge: true,showHeight: false);
  // person._name = "Sami";
}
