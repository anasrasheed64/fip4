import 'dart:math' as math;

void main() { // function
  print('Hello Every Body'[6] + 'nas is the best'.indexOf('e').toString()); // E9
  print(('Hello Every Body'[6] + 'nas is the best').indexOf('e').toString()); // E9
            // (Enas is the best).indexOf(e) 10
  print(10 * 5 > 5 * 5 + 25 * 1);
  String international="International";
  print(international.substring(international.length-5));
  print(international.substring(8));// hard coded
  print("Hello World");
  print("Ahmad");
  print("Ahmad");
  print("Ahmad");
  print("Ahmad");
  print("Ahmad");
  print("Ahmad");
  print("Ahmad");
  print(math.max(10, 20));
  print(5);
  print(10 * 20);

  // data types
  // String int double bool Object

  // String Data Type (String support String pool)

  print('this is string'); //   ''   ""   anything
  print('10*20');
  print("Hello" + " Abed"); // concatenation دمج
  print("Hello" " Madhat");
  print("Hello Madhat");
  print('a');
  print("a");

  // int 4 byte

  print(5); // 5
  print(10 + 5); // 15
  print(15 + 25); // 40
  print(20 - 5); // 15
  print(10 * 2); // 20
  print((10 / 2)); // 5.0

  // double

  print(5.3);
  print(1.5 * 2); // 3.0
  print(4.5 / 2);
  print(5.8 - 3.2);
  print(8 / 2); // 4.0

  //bool
  print(5 > 3); // true
  print(5 == 4); // false
  print(4 <= 3); // false
  print(4 <= 4); // true

  // End of Prints

  // create new variable
  // [data type]  [variable name] ;/ = [number];
  String name="as"; // declare variable  تعريف  null   {null - null safe} 123
  print(name); // null
  print(name.toUpperCase()); // exception
   // pointers oop
  // print(name.length);  Unhandled Exception: NoSuchMethodError: The getter 'length' was called on null
  name = "anas"; // assign initialization
  print(name);
  // ahmad.name(); // heap 123.name
  String x = "Hello"; // declaration + initialization
  print(x);

  print(x + name); // concatenation
  print("Good Morning $name"); // concatenation
  print("Good Morning $name"); // concatenation

  // String methods

  print(x.length); // 5
  print(name.length); // 4

  print(x.isEmpty); // false
  print(name.isEmpty); // false
  print("".isEmpty); // true

  print(x.isNotEmpty); // true
  print(name.isNotEmpty); // true
  print(''.isNotEmpty); // false

  print(x.indexOf('H')); // 0
  print(x.indexOf('o')); // 4
  print(x.indexOf('l')); // 2
  print(x.indexOf('k')); // -1 not found       / Hello

  print(x.lastIndexOf('l')); // 3
  print(x.lastIndexOf('H')); // 3
  print(x.lastIndexOf('h')); // -1  case sensitive

  print(x.endsWith("o")); // true
  print(x.endsWith("O")); // false
  print(name.startsWith("A")); // false
  print(name.startsWith("an")); // true
  print(name.endsWith('anas')); // false   length = name=.length;
  print(name.endsWith('nas')); // true

  print(x.toLowerCase()); // hello
  print(x.toUpperCase()); // HELLO
  print(x); // Hello
  x = x.toUpperCase();
  print(x); // HELLO

  // H E L L O
  // 0 1 2 3 4
  print(x[0]); // H
  print(x[4]); // O
  // print(x[8]); // O  // index out of range  Exception

  print(x[0] + x[3]); // HL

  print(" anas is man ".trim()); // removing the right and left extra spacing
  print(" anas is man ");
  print(" anas is man".trim());
  print(" anas is man".trimLeft());
  print(" anas is man".trimRight());
  print(" anas is man h".trimLeft());
  print(" anas is man h"
      .replaceAll('h', '')
      .trim()); //" anas is man "  anas is man
  print(" anas is man h"
      .replaceAll('a', 'b')
      .trim()); //" anas is man "  anas is man
  print(" anas is man h"
      .replaceAll('k', 'b')
      .trim()); //" anas is man "  anas is man

  print(x.contains("LO")); // true
  print(x.contains("HELLO")); // true
  print(x.contains("anas")); // false
  print(x.contains("HELL")); // true
  print(x.contains("hello")); // false

  print(x.replaceFirst("L", 'e')); // HELLO HEeLO
  print(x.replaceRange(1, 3, "Anas")); // HELLO  , Anas
  print("0123456789".replaceRange(5, null, "Anas")); // HELLO  , HELAnas 012Anas
  String numbers = '01234567892';
  print(numbers.replaceRange(
      5, numbers.length, "Anas")); // HELLO  , HELAnas 012Anas

  print(x.substring(3)); // HELLO   LO
  print(x.substring(1)); // HELLO   ELLO
  print(x.substring(1, 3)); // HELLO   EL   end index not included
  String sub = x.substring(0, 3); // HEL
  print(sub);

  //  int methods    () ^ */ +-
  int y = 10;
  print(y * 2); // 20
  print(y + y * 2); // 30
  print(y / 2 + y * 2); // 25.0
  print((y - 5) * 2 + y * 2); // 30
  print(y.isEven); // true
  print(y.isOdd); // false
  print(y.isNegative); // false
  print(y.toDouble()); // 10.0   casting
  print(y.isNaN); // false
  print(math.pow(y, 3));
  print(math.min(10, 20)); // 10
  print(math.max(10, 20)); // 10

  // double 8 byte

  double d = 15.9;
  print(d + d); // 31.0
  print(d * 2); // 31.0
  print(d / 5); // 3.1
  print(d.toInt());
  print(d.ceil()); //
  print(d.floor()); //
  print(d.round());
  print(15.4.round());

  int c = 15.4.toInt(); // 15
  int k = 15.4.floor(); // 15
  print(c);
  print(k);
  c = -15.4.toInt(); // casting
  k = -15.9.floor(); // not usable in casting
  print(c);
  print(k);

  String p = y.toString(); // casting
  print(p);
  p = (-25).toString();
  print(p);
  print('${p}5'); // -255

  // boolean

  bool isMale = true;
  bool z = false;
  print(isMale); // true

  bool isCharFound = "Anas".contains("a"); // true
  print(isCharFound);
  isCharFound = "Anas".contains("b"); // false
  print(isCharFound);

  // name
  // variable name must start with letter or _  ,
  // variable name must be unique and valuable
  // naming convention  (lower camel case)
  String anas;
  String anas2;
  String ana2s;
  String databaseNameOfOracle = "";
  String databaseName;

  // var dynamic

  var r = "anas"; //String
  print(r);
  dynamic oo = "anas"; // String 16
  print(oo is String);
  print(oo);
  r = "tala";
  // r = 25; error
  oo = 25;
  print(oo is int);

  oo = false; //1 bit
  print(oo is bool);

  var rr = 5; // 4 byte
  dynamic ii = false;

  print(r is String);
  print(r is int);
  print(y is double);
  print(y is int);
  print(oo is String);
  print(oo is bool);

  // conditions

  //simple  if (condition) { }

  if (5 > 3) {
    print('success');
  }

  if (3 < 1) {
    print('hello world');
  }

  // if else   if(condition) { } else { }

  if ("anas".endsWith('As')) {
    print('anas is ends with AS');
  } else {
    print('anas is not ends with AS');
  }

  if ("anas".endsWith('As')) {
    print('anas is ends with AS');
  } else {
    if ("anas".endsWith('AS')) {
      print('anas is ends with AS');
    } else {
      if ("anas".endsWith('as')) {
        print('anas is ends with as');
      } else {
        print('anas is not ends with AS and As');
      }
    }
  }

  if ("anas".endsWith('As')) {
    print('anas is ends with AS');
  } else if ("anas".endsWith('AS')) {
    print('anas is ends with AS');
  } else if ("anas".endsWith('as')) {
    print('anas is ends with as');
  } else {
    print('anas is not ends with AS and As');
  }

  int numberOfStudent = 3;

  if (numberOfStudent == 1) {
    print('hello Aseel');
  } else if (numberOfStudent == 2) {
    print('hello aseel and tala');
  } else if (numberOfStudent == 3) {
    print('hello aseel and tala and yazan');
  } else {
    print('hello everyone');
  }

  // switch (variable (check)){
  // case {constant} : {} break;
  //
  // }

  switch (numberOfStudent) {
    // 3
    case 1:
      print('hello Aseel');
      break;
    case 2:
      print('hello aseel and tala');
      break;
    case 3:
      print('hello aseel and tala and yazan');
      break;
    default:
      print('hello everyone');
  }

  if (numberOfStudent > 5) {
    print('hello 5 body');
  } else {
    print('hello bodies');
  }

  // short confition

  print(numberOfStudent > 5 ? "hello 5 body" : "hello bodies");

  // for loop
  print(1);
  print(2);
  print(3);
  print(4);
  print(5);
  print(6);
  print(7);
  print(8);
  print(9);
  print(10);

  print(numberOfStudent);
  numberOfStudent = numberOfStudent + 1;
  print(numberOfStudent);
  numberOfStudent = numberOfStudent + 10;
  print(numberOfStudent);
  numberOfStudent = numberOfStudent - 10;
  print(numberOfStudent);

  //numberOfStudent 4
  // prefix suffix
  print(++numberOfStudent); // numberOfStudent=numberOfStudent+1 5 prefix
  print(--numberOfStudent); // numberOfStudent=numberOfStudent-1 4 prefix
  print(numberOfStudent++); // numberOfStudent=numberOfStudent+1 4 suffix
  print(numberOfStudent); // 5
  numberOfStudent = numberOfStudent + 3;
  print(numberOfStudent); // 5
  numberOfStudent += 3;
  print(numberOfStudent);
  numberOfStudent -= 7;
  numberOfStudent *= 7;
  numberOfStudent = (numberOfStudent / 2).toInt();
  double b = 55.5;
  b /= 3;
  // index

  // for  (**startCounter**;<condition>;**increase/decrase**){ optional
  //
  // }
  print('****************** start for loop ********************');
  for (int counter = 1; counter <= 10; counter++) {
    print(counter); // 1 2 3 4 5 6 7 8 9 10
  }

  print('****************** start for loop 2 ********************');

  int startCounter = 1;
  for (; startCounter <= 10; startCounter++) {
    print(startCounter); // 1 2 3 4 5 6 7 8 9 10
  }
  print(startCounter); //

  startCounter = 1;
  for (; startCounter <= 10;) {
    print(startCounter);
    startCounter++;
  }

  // while
  // while (condition){
  //
  // }
  print('****************** start for loop 3 (while) ********************');

  startCounter = 1;
  while (startCounter <= 10) {
    print(startCounter);
    startCounter++;
  }

  for (int counter = 0; counter <= 5; counter++) {
    if (counter == 3) {
      break;
    }
    print(counter);
  }

  // 1 -10
  // even 1-10

  // 1 - 100  10 20 30 40 50 60 70 80 90 100
  for (int counter = 1; counter <= 100; counter++) {
    if (counter % 10 == 0) {
      // 5/2 = 2 1    10/3 = 9 1   10/4 = 2 2 10 20 30 40 50 60
      continue;
    }
    print(counter);
  }

  // looping

  // do while

  while (3 > 5) {
    print('3>5');
  }

  int counter = 3;
  do {
    print(counter); // 3 2 1
    counter--;
  } while (counter > 5); // infinite loop

  while (false) {
    // infinite loop
    print('ahmad');
  }

  // collections  list map Stack Queue LionkedList
  int v = 5;
  List<int> numbersOfClass = [1, 5, -20, 80, 82, 100, -542, 156, 22]; // index
  // List<int> numbersOfClass2 =[];
  print(numbersOfClass);
  // print(numbersOfClass2);

  print(numbersOfClass[3]);
  // print(numbersOfClass[10]); //  error
  print(numbersOfClass[0]); // 1
  print(numbersOfClass.length); // 8
  print(numbersOfClass.isEmpty);
  print(numbersOfClass.isNotEmpty);
  print(numbersOfClass.first); // 1
  print(numbersOfClass.last); // 22
  print(numbersOfClass.contains(-20)); // true
  print(numbersOfClass.contains(-95)); // false
  print(numbersOfClass.elementAt(0)); // false
  print(numbersOfClass[2]); // false
  print(numbersOfClass.indexOf(-99)); // -1

  numbersOfClass.add(-80);
  print(numbersOfClass);
  numbersOfClass.insert(0, -80);
  print(numbersOfClass);
  numbersOfClass.insert(5, -97);
  print(numbersOfClass);
  numbersOfClass.remove(-97);
  print(numbersOfClass);
  numbersOfClass.remove(3);
  print(numbersOfClass);
  numbersOfClass.removeAt(0);
  print(numbersOfClass);
  // numbersOfClass.removeAt(20);
  // print(numbersOfClass);
  numbersOfClass.removeLast();
  numbersOfClass.removeRange(0, 3);
  print(numbersOfClass);
  numbersOfClass.addAll([5, 1, 7, 20, -80, -71]);
  print(numbersOfClass);
  List<int> class2 = [0, 0, 0];
  numbersOfClass.addAll(class2);
  print(numbersOfClass);
  numbersOfClass.insertAll(1, class2);
  print(numbersOfClass);
  numbersOfClass.replaceRange(0, 2, [8, 8, 25, 10]);
  print(numbersOfClass);
  print(numbersOfClass.lastIndexOf(0));
  print(numbersOfClass.indexOf(0));
  print(numbersOfClass);

  for (int index = 0; index < numbersOfClass.length; index++) {
    print(numbersOfClass[index] + 5); //8 8 25
  }
  print(numbersOfClass);
  numbersOfClass[0] = 15;
  print(numbersOfClass);

  for (int value in numbersOfClass) {
    // foreach
    print(value + 5);
  }
  List<int> data = [10, 20, 30];

  // create list of int and insert any random 10 numbers
  // remove third element ex: {0,1,2,3,4} after remove [0,1,3,4]
  // remove last
  // remove first
  // add all [1,1,1,1]
  // insert new number in index 3 {-100}
  // insert list of number in last index [99,99,99]
  // print even numbers in the list
  // print only odd numbers in the list increased by 2

  List<int> marks = [1, -52, 97, 17, 16, 4894, 48, 12, 0, 87];
  marks.removeAt(2);
  marks.removeLast();
  marks.removeAt(0);
  marks.addAll([1, 1, 1, 1]);
  marks.insert(3, -100);
  marks.insertAll(marks.length - 1, [99, 99, 99]);

  for (var value in marks) {
    if (value % 2 == 0) print(value);
  }

  for (int index = 0; index < marks.length; index++) {
    if (marks[index] % 2 == 0) print(marks[index]);
  }

  for (int number in marks) {
    if (number % 2 == 1) {
      print(number + 2);
    }
  }

  // for loop to print all elements reversed
  // [1,2,3,4,5]  4
  print('reversed ******');
  for (int index = marks.length - 1; index >= 0; index--) {
    print(marks[index]);
  }

  // list
  List<String> names = [
    "Ahmad",
    "Ali",
    "sami",
    "Tala",
    "hala",
    "Batool",
    "abcdef"
  ]; // Muhannad
  // names.add(5); // error
  names.add("Muhannad");
  names.add("Muhannad");
  names.add("Muhannad");
  names.add("Muhannad");
  names.add("Muhannad");
  print(names.length); // 7
  print(names.indexOf("Ali")); // 1
  print(names.elementAt(2)); // 2
  print(names.contains("anas")); // false
  print(names.contains("ali")); // false case sensitive
  print(names.contains("Ali")); // true case sensitive
  print(names.first);
  print(names.last);
  List<String> namesResult = [];
  // for (var value in names) {
  //   if(value.length==4){
  //     namesResult.add(value);
  //   }
  // }
  // print(namesResult);
  namesResult = names.where((value) => value.length == 4).toList();
  print(namesResult);
  String res = names.firstWhere((element) => element.length == 6);
  print(res);
  print(names.lastWhere((element) => element.length == 6));
  // print(names.lastWhere((element) => element.length == 20)); error bad state No element
  print(names.any((element) => element.length == 4));
  print(names.any((element) => element.length == 20));

  bool isFound = names.any((element) => element.length == 6);
  if (isFound)
    print(names.firstWhere((element) => element.length == 6));
  else
    print('no element with length 20');
  print(names);
  names.removeWhere((element) => element.length > 6);
  print(names);
  names = names.reversed.toList();
  print(names);
  bool isAllElementLengthGreaterThan4 =
      names.every((element) => element.length >= 3);
  print(isAllElementLengthGreaterThan4);
  List<String> subList = names.sublist(3, 5);
  print(subList);
  names.clear();
  print(names);

  //collections  Map     element{ Key , Value }
  List<String> yy = [];
  Map<int, String> map = {
    1: "Anas",
    2: "Tala",
    3: "Moahmmed",
  };
  print(map[1]); // anas
  print(map[3]); // anas
  print(map[2]); // anas
  if (map.containsKey(5)) print(map[5]); // null key not exist
  if (map.containsKey(1)) print(map[1]);
  print(map.containsValue("Tala"));
  map.putIfAbsent(4, () => "Sami");
  // map.putIfAbsent(4, () => "Sameer");
  map.putIfAbsent(100, () => "Fatima");
  map.putIfAbsent(-100, () => "Fatima");
  map[100000] = "Sameer";
  print(map);
  // functions
  //O(n)
  // O(1)
  for (var element in map.values) {
    print(element);
  }
  for (var element in map.keys) {
    print(element);
  }

  map.forEach((key, value) {
    print(key.toString() + value);
  });
  map.removeWhere((key, value) => key > 1000);
  print(map);
  map.removeWhere((key, value) => key < 0);
  print(map);
  map.addAll({55: "Tareq", 64: "Omar"});
  print(map);
  // map.update(-55, (value) => "Anas2"); // error
  map[-55] = "Anas3";
  map.updateAll((key, value) => "$value StepByStep");
  print(map);

  // map.clear();

// to reformat your code (ctrl + alt + l)
// to remove unused imports (ctrl + alt +o)
// to rename used variable shift + f6  --- shift + fn +f6
// please meshan allah variable name must be meaningful

  // create list of names contains [ ali , ahmad , anas , muhanned , basil , batool , tareq]
  // add any new 3 names .add
  //print if name batool is exist
  // print numbers of names that have length more than 5
  // print all names that have 3 length
  // remove all names that ends with d
  // add new list to the current [jamal , saeed , jameel] addAll
  // remove all names that have more length more than or equal 6
  // print all items
  // clear the list and print it
  names = ["ali", "ahmad", "anas", "muhanned", "basil", "batool", "tareq"];
  names.add('Kahleed');
  names.add('Mahmoud');
  names.add('Khaldoun');
  print(names.contains("batool"));
  print(names.where((element) => element.length > 5).toList().length);
  print(names.where((element) => element.length == 3).toList());
  names.removeWhere((element) => element.endsWith("d"));
  names.addAll(["jamal", "saeed", "jameel"]);
  names.removeWhere((element) => element.length >= 6);
  print(names);
  names.clear();
  print(names);

  //
  // create a map that have String key and int value
  // the key is the person name
  // the value is person age
  // enter these value for init
  /*
  Name : Anas   Age 25
  Name : Toleen   Age 18
  Name : Saeed   Age 29
  Name : Saleh   Age 35
  Name : Feras   Age 14
  * */
  // add any 3 new persons .putIfAbsent
  // print if name Feras if exist and print it's value if it exist
  // remove all names their ages greater than 32
  // print all names that key length is more than 4
  // print all ages that their names starts with S
  // remove all names that ends with d
  //print all keys
  //print all values
  // update all names their length is 4 to value 30
  // print the map
  // clear map
  //print map again
  print('*************************');
  Map<String, int> persons = {
    "Anas": 25,
    "Toleen": 18,
    "Saeed": 29,
    "Saleh": 35,
    "Feras": 14
  };
  persons.putIfAbsent("Tala", () => 20);
  persons.putIfAbsent("Raghad", () => 46);
  persons.putIfAbsent("Tasneem", () => 7);
  if (persons.containsKey("Feras")) {
    print("feras age is ${persons["Feras"]}");
  }
  print(persons);
  persons.removeWhere((key, value) => value > 32);
  print(persons);
  persons.forEach((key, value) {
    if (key.length > 4) print(key + value.toString());
  });
  persons.forEach((key, value) {
    if (key.toLowerCase().startsWith("s")) print(value);
  });
  persons.removeWhere((key, value) => key.endsWith("d"));
  print(persons.keys);
  print(persons.values);
  persons.forEach((key, value) {
    if (key.length == 4) persons[key] = 30;
  });
  print(persons);
  persons.clear();
  print(persons);
  /*
   Map<int, String> map = {
    1: "Anas",
    2: "Tala",
    3: "Moahmmed",
  };
  print(map[1]); // anas
  print(map[3]); // anas
  print(map[2]); // anas
  if (map.containsKey(5)) print(map[5]); // null key not exist
  if (map.containsKey(1)) print(map[1]);
  print(map.containsValue("Tala"));
  map.putIfAbsent(4, () => "Sami");
  // map.putIfAbsent(4, () => "Sameer");
  map.putIfAbsent(100, () => "Fatima");
  map.putIfAbsent(-100, () => "Fatima");
  map[100000] = "Sameer";
  print(map);
  * */

  // [10,20,05,88,84,-4,15,1,100,156,984]

  // write a code to find the max number
  List<int> ages = [20, 17, 35, 11, 98, 85, 100, 24, 1, 153, 111];
  print(findMaxFromList(ages));

  List<int> ages2 = [20, 17, 35, 11, 98, 85, 100, 24, 500, 153, 111];
  print(findMaxFromList(ages2));

  List<int> ages3 = [20, 17, 35, 11, 98, 85, 100, 24, 1, 153, 200];
  print(findMaxFromList(ages3));

  List<int> weights = [];
  // print(findMax(ages)); // 153
  //
  // List<int> ages2 = [20, -20, 35, 11, 98, 85, 100, -24, 0, 153, 111];
  // print(findMax(ages2)); // 153
  //
  // List<int> ages3 = [20, -20, 35, 11, 98, 85, 100, -24, 0, 153, 111];
  // print(findMax(ages3)); // 153
  //
  // print(findMin(ages));
  // printList(ages);
  // int myMax=max();
  // print(myMax);

  int i = findMaxNamed(ages);
  print(isListBig(ages));
  List<int> sameer = [2, 3];
  print(isListBig(sameer));
  print(isListBig([2, 3]));
  List<String> stepbystepNames = [
    "Kamal",
    "Mohammed",
    "Medhat",
    "Mohannad",
    "Reem",
    "Saeed",
    "Tala",
    "Anas"
  ];
  // List<String> stepbystepNames=["Kamal StepByStep","Mohammed StepByStep","Medhat","Mohannad","Reem","Saeed","Tala","Anas"];
  print(correctNames(stepbystepNames, "StepByStep"));
  List<String> xNames = [
    "Kamal",
    "Mohammed",
    "Medhat",
    "Mohannad",
    "Reem",
    "Saeed",
    "Tala",
    "Anas"
  ];
  print(correctNames(xNames, "XAcademy"));
  printPassedNames(stepbystepNames);
}

List<String> correctNames(List<String> names, String name) {
  for (int index = 0; index < names.length; index++) {
    names[index] = "${names[index]} $name";
  }
  return names;
}

void printPassedNames(List<String> names) {
  for (int index = 0; index < names.length; index++) {
    print((index + 1).toString() + " ${names[index]} is Passed");
  }
  return;
}

// function / method
// Box
int findMax(List<int> list) {
  //  arguments , prameters
  int max = list[0];
  for (int item in list) {
    if (item > max) max = item;
  }
  return max; // 153
}

int findMin(List<int> list) {
  //  arguments , parameters
  int min = list[0];
  for (int item in list) {
    if (item < min) min = item;
  }
  return min; // 153
}

int max() {
  return 10;
}

void printList(List<int> list) {
  print("my list is$list");
}

// methods can take 0 arguments
// methods can take multi arguments
// return type  (data types) , void (does not return anything)
//

// output return type(int)  name ( parameters  input  )  {   }
int findMaxFromList(List<int> list) {
  int max = list[0];
  for (int age in list) {
    // foreach
    if (age > max) max = age;
  }
  return max;
} // parameters arguments
// method , function

int findMaxNamed(List<int> data) {
  return 5;
}

bool isListBig(
  List<int> data,
) {
  return data.length > 10;
}
