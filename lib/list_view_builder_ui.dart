import 'package:fip4/list_view_builder_hori.dart';
import 'package:fip4/place_card_view.dart';
import 'package:flutter/material.dart';

import 'grid_view_builder.dart';
import 'models/place_model.dart';

void main() {
  runApp(const MyAppView());
}

// stl stf
class MyAppView extends StatelessWidget {
  // upper camel case
  const MyAppView({Key? key}) : super(key: key); // constructor
  // lower camel case
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "batata",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        cardColor: Colors.grey[600],
        scaffoldBackgroundColor: Colors.white,
      ),
      // theme: ThemeData(
      //     primaryColor: Colors.orange,
      //     scaffoldBackgroundColor: Colors.white,
      //     colorScheme:
      //         ColorScheme.fromSwatch().copyWith(secondary: Colors.white),
      //     backgroundColor: Colors.red,
      //     splashColor: Colors.black),
      home: GridViewBuilder(),
    );
  }
}

class ListViewScreen extends StatefulWidget {
  const ListViewScreen({Key? key}) : super(key: key);

  @override
  State<ListViewScreen> createState() => _ListViewScreenState();
}

class _ListViewScreenState extends State<ListViewScreen> {
  List<PlaceModel> _places = [];

  @override
  void initState() {
    _fillPlaces();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            title: Text("Cities"),
            centerTitle: true,
          ),
          body: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "Cities Around the world",
                  style: TextStyle(color: Colors.red, fontSize: 30),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 30,
                ),
                ListView.builder(
                  itemBuilder: (context, index) {
                    return PlaceCardView(
                      model: _places[index],
                    );
                  },
                  itemCount: _places.length,
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                )
              ],
            ),
          )),
    );
  }

  void _fillPlaces() {
    _places.add(PlaceModel(
        name: "London",
        base64Img:
            "/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxQUExYTExQXFxYYGR8ZGRgYGBsbGxkbHhscHyAhHxsgHykhIB4mHyEgIjIiJiosLy8vGSE1OjUuOSkuLywBCgoKDg0OHBAQHC4mIScwMC4uLjkuOTEuLjAwLi4uLi4uLi4wLi4xLi4uLi4uLi4uLi4uLi4uLi4uLi4uLi4uLv/AABEIAI4BYwMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAFBgMEAAEHAgj/xABIEAACAQIEBAMFBAgDBgQHAAABAhEDIQAEEjEFIkFRBhNhMnGBkaEUI0KxBxUzUmLB0fBykuFDc4LC0vEkU6KyFjRjZHSjs//EABkBAAMBAQEAAAAAAAAAAAAAAAECAwAEBf/EAC8RAAICAQMDAQcDBQEAAAAAAAABAhEDEiExE0FRBCJhcYGRocEysfAUI0LR8VL/2gAMAwEAAhEDEQA/AO0RjIxvGYYJqMbjGYzGMaOMjG4xmMYzGifTHrGYxjyMbxvGYxjWN4jFVZ0yNW+mbxJvHbEmMYzGYzGicYxvGsbGMxjGl9d8ZjMYzRc4xjMZiBMxJttjHqNNtsag0TxiDK5qnUGqlURwDEowYTvEg7wR88aqZxEKh3VSxAAZgCSWCiJ3liB72A6451+g6v8AdZhP/rB/mig/y/sYVvegqOzZ02MZjeMwwpqMZGN4zGMajGoxjMBucefOGCaj1GMjGhUHfGjVXvgGo3GMjHkVhiTGs1HiMZGPeMxrBR4jGse8ZGDZqPMYyMeoxkY1mo8RjIx7jGRjWCjxGMjHqMZGNZqPEYyMe4xqMazUeIxmPcYzBs1HrGY1ONHExqPRONK3pjwq9zOPc4JqPWMxqcaDYBj1jManGTjGPWMGPM4G57jS0206HYz00gdOpOA3XIUrOV5jiRFao5dwVzRPKxUkK5tYibWg77HHXuG55a9JKqAhXGoBgAY9YJH1x88eIeJsmYULEVMxUBnvrUD8746t+j6owdkJMCmbGbEMu0+84ydJDRi5W/A94Rf0quMxlauQpn7+p5ZUNZYFRWu3uU4eC0Y594neeIIQfwL9NWEzZNEbGw41OVHB+O8GzOSZBUbSXkqUc7Cx7d8fV3BZOXokkkmkhJNyToG5xynxj4bo5o02qvUBRXA0FRbUvdT3x0HwjxM1E8ogfdooB7jYfIDCYsykvePlwtW+wxYzGTjJxchRgGMwr+PfFgyGWatALnkpqT7TnaQLwBLH0GFv9GP6QamcpumY0mvTM6gANaE2MCwINjHdfXDKLBsEv0kJL5Y7EVqUHqD5ymZ6XA+WFj9CShVeqZlnamb8seXTe/8AFIPzOGHxxmxUah0Hm0v/AOgwr/opQDL+YQOWtfYHTopyI62xCb0y3K41aZ2NakiRBxoVe+IKWap9tPvgYmKg3EX64vVE00eKueVcUn4segGJKrKDBUMfS+J1SmvMQAd4PT4YfZdjOiiqVWvBPvOMelUm4j44JK5g29wxQfMsPaONqbHg3wjBSPU4901X1xUrcSG2PFPNAHlJPex/LBp0V7hgkIsnYYr/AKw7Yq/bT2Yj/DjKbMdkgeuFS8g0oIU83PTFkON8U0AEXv8ATAvi3EFBEuqgGLzvE9u18BU2JKHgYFcHY42GGFlc8rW8+nvAgjf44IrljHtmfzwWkuWBQCs48mqBacVFdgACMbNYC9sKbpsuBhjQfFT7YN5tjynEFODpYmllupUgbE+7HmlWncEYi+2Lj2qrFhvjAaaPRzC98bWsDscQigvb8sZoS9h6x/TG2AWJxmK/mr/YOMwKNZJ5mM14Efbz2+uPS573YfpsfXHyFteN68Cvtwxr9YDA6bNrj5C2vG9WBI4gMehnhjdNm1R8hTVjNWBv2wd8Z9swNDDaCYbHLaGeqO4ZnJvc8xn3kjsPph3zefiGAJ0mbfI/QnHMcvVZHWYBsbACxUkHbYjY+oxDI+Y9zoxY7WrsIXiCoJpTdjUeDexLJfffrfHY/wBF8zLSSaZkmLnWs7WxxrjPD67GnFKqdNRjyo2xYQQQNjBgi1sdZ/RnmoQapVtBBDGTOv3npfBjHgVy3l2VHSczJEDHPvEpK51OUmKc2/wue+HL7aO+FjjNE1M0GHVCPT9m+I+t9nGk/P4KejpzfwKXFah0qdJ/GNv4kwR8EViPNgSYUfVsa4lkWgCOrfVh/TGeFq6UxUJqICSBGpZsGJ69McnpZe2o13OjOl027G9azRc39BIH+uI2qVCsKb9zbFX7VIkEEEwOoJ7epx4Oag6Sb9pg/LHtKJ5LaOP/AKahVfM0KLOCgX2QT7Zkk+p0hQO1++AfgXKvRz9IBlEiCZMODpDj82HuXF/9IuYNatWfrSYkHsUZv5D/ANQwH4b4mR6lP7rTUWoaiFTYEJAW5JIbTB/xnsMNLTHdk4OTdL4HYvFmV0vl1YzNWn7vbwrfo1qsMqw0oQam5ZtV0T+GI+uJh4jXiaK6h6QSoq7gtqBUyIO3MOo2OKvgEH7OTrqIPMNlRyPZQbj8seR6jJd/E9LDConY8vVDorQOZQ3zE4ifLyANZA7DAnhWb+7AkmJEkFSb9jfbFls0emPSx3KKku5yT0ptFyhk1U6pJOLI09hgUuZPXFHxBxVqOWrVkgtTps41TEgE3AIMfHDST5YsWnsgxnc9p9mJwBqZ0dccjzv6UM2TOnLz6U2/nUOGf9HviupnnrLWpUAKaqR5dNhdid9TN2wcU4AnCS7jTWz4Np+kY8JxJlFj9MW2ytM/7MT3jET5QdAPljouL5RNSnHdMhHFmH731xicacfvHGMgG4PW+kkW9YtjQggMLgiRGxB22xqh4D1snk9p4gYbq31wteJS+YJinbWd4mDTpxv7n+mGArPTEC2d9UXVDf0NTb5jE8kYJauCkM05OuRKynhmvqJVCLbqyreQDsR2nDDwjJ52l7OsehZSPiVaSbjecGV8RZajPmulzywCTHrAxG36R8qswlRh3VF/mwxy/wBRFPu/l+S2mdcJfUPVM++lQdUlZI+J/wC/xxF5xP8AqceM05LaiTcdouOU26XBxGHPfHTjSlFSRKeZqVMvIzRfbE9GlJmQPcMDlY+mJNR/ew2kR5vAXpBVO/5YmNYdSPQYCq3dvljYYdz88LoEeQO68YHGAq5lRaT88SrX64HTYOogt5mMwL+0euMwNDDrQD/WK9Z+Rx5/WiWvuY+OObZ3x1TIQN53KoU8qwettRBMEHeLg7490PFdDTaTIHthywItPLTKk7dYtaN8VUrQHFJ8nTKmeUMVk2/oDiP9Zp3OOcZXjprVAVZneYBkKJAJuCAdhP0GL9bP+UpLtDuItLFjJtPTp3wVtyCTi3cR5HE07/THocTpzGoT2vjmbcWkC5Cgap5tzG56GIx7PHm/dtGkKQ9vZM7gzLG/p6WR5YjwxpvcfKHFimYbUwNMrIBGxAY9rWWb4dHNL9xd427mPzxxnLcY8zUTysqkCDc/d1RPwJE++cdMrZtNta+2IlgASKwUL8SQB64871cpppwbOvDCMlTXB54nURbyAGmL2+B92OLV8yWKgxMW5gSbgSbnv1j6Y6rxjxAKPkpKQ4DTqI9kjTACkEGAN5kixxzXM8KSm9BjY1NgoDagrICTL7TYW6HeMNHK3GLnzTLxx6YS0+V+zH1stVWll9Sjlo06RvMMhg7DaTv/ACnCX4HzanNqRYMzH/8AW8dcdTpZGlogU1AABF+pE9+5nHCPCmYdGpmmhZz7AAJJ+7MwOtp+WH9LPVbo5/VQScVZ3X7Sv7w+Ywj5/jLjiwprVYItOdIPL+xY7bdceMrnKigNmsvmNIWB5MM5cEAEoxgKTM33G2FKrm54mXBMaLSb3o9enXD+rWrHdA9PUMlJ+46pxLibSnMRJYR3uvyuccr8OuTVaC+grUPqNNJipkC7co77nDVxLOSaEHdm/wDdR/rhW4TWmo0JslcgAnbyah6JHWLfXHn+kTSbZ0+tq4pDI1SDHmVBJBuSOv8Ah3uMW+C5uK9KXaSdRDA2lG6xEyNp+GANKsjm6ERvqB6sSIBp9xPy9+PKcQVR5uhgBMcpm6kD8INiwO/e1sdsZPUjipUVPFGY0183Bs+t0nqdFPV073HuOE/KcRZaiOTsQx9IM/l+eLnFKrVKa1SSQSQ5ECGYWBuZsnYewb9gJwZJSVM0W4O18TpHhfiSUS9E1FSKxiTAVABzT2gC+LfDs5Ty8UVrUKsy4anzAWAI3BBETtt8QEXMEFlc/jphuw1AaT9VPz9MTZWtFQaQQL9Sfwrva4m+3X0xyPDF7eTuWVrc7Z4O4stRaqAiVZTAMxrB/wCk4PnMD++uOR+Da6q9RHMA0mYEEzIIYdgbarT78HeJcfpUxyHVENp0npeNcQdTQJ6A9cd2FqOOl2OPLHVPV5HCpxpVTUysCWKimYDsQYspN5kW9cD/ABhmnal9nRWY10dWVELuEhQWABm2odCLj4oVHi75iqrOP2bVdyGJZqTt1MiNESBHzwToeJDUelWRGP3RUMH0leVSw02sfLW47AdTieTNLTvSKY8MHLZnOeJ5dKVVqbM4iLPSIdZI3UsL6TMfD1x0L9D2UVGrMpcipoVWZNKmA5MXMx1g2kYpeMclRrZatmCsVUNIa9oDOQbAwdydsXP0T5bXRqFW8tkKsHUBi16u4MbXAud+mF6iilJ/MZ4m5uP0OhVM2BEkCTA9T2xV4nnCtMkEgyBIMH4RhSz9cuq06bl6lIo9RZLOBtBgWJDfQmcWxxZas6k0lPLuWAHOzoQQxCwApNyPag2x1PIqOfou6sbuE54HLtzAnSSbyRK2nrt/LA7LcSSqFCtJVQGEyR0FvUD5zijk6yIkVKy1CrAq7NRVlUrDryOJVmBaDYE/DAjj3Dvs+ioaqNqEkXBC6SQxIUze3aesScQxzj1NyuWDWPYNpxZWAIES2nf1I/l9cDvFNQhCbx5ZmO/m0Y/M/PC9xGjXy7ojSCQlRShYgmFnTabE7+oxOc4tSiUUgxS5jHMXV01Am1gRa098XlL2GuSKilNPihWzjFiSQN7Sf7tghwPLpUrrRYAKzoAN9QZyN4n0+eKObpKd/wAuvbf8sX/C2SDZqiIgc14G/lkz8GFvfjlRdu2dTp53zEDXszAyIvPT0mfrjw2cVd3Ue9gMAeI1EWtUV6ZNMMZgMNRV2BGoC8yRP8Pe+KOWfLN5jM8BVMKFNmIYJzapsbxsfhjshlilTX8ZzZfTuTuL/iG0Z9f31/zDGfrJP30/zD+uEirncstMgM5cMApJOkrDajtYTptEi9jitQ4irLuqm4IZh9YUTMb2/PFeoiDwtbWPg4xSO1VD/wAQ/rjf62p/+an+Zf64RmZTzq6x+GDHU3nTFoiAZkgYlRKjrqUq7TFkpxF+pAk9cZZEK8Mhy/XCbCon+cf1x7GcJ2M+44SjwxwhZnp7gD7xZJN+0fMjbFQuyDUaluUQYkzExHa4werEX+nkdB+1NjeEdq2nlNSCO6t/XGYbqRB0JeRMzLt5aKIDAEHnQE/+q98VUquJ+8P+cH8pxvN5RywAAblEcoB2vbVNjPwGLLZFFpAvTIaOZiKggnYjZbTG8SMcyx77HQ8vsqyLJZrS/O7Emwgnc9jpsTMfHDDxXiFCt5S+XmVFLqVksgCyJ1DmJG8nvhZfhFUIKiiL7+ZTAMRBgtMzP0wTzNM+1AHMfZKmZIvFzP8Ac4zbihbRbzucyugrToV1M/iDHlFujDFajmKZ9tqo6DTRLe/dx16euKyUrTpDCGs2kbtPSD/fXfHpUVSfMUAEyLTHN0iPrONptboTq09mEMrnaNJW0moTBgGkVWBI31Hm+h2vY4KZbj71yVohCyAOBUOjWyuKh0AnUwAU2A7yIjC9UpIaRaFm4mDJGuO/X3YgymY8tmKVaijTvTJU29A8G/RrHCzWlFceTXLcMZzxTXr+UrUGphFFOQGuJG/KI26HFvOcT86pl/uxTWmYJ1gzqZDcG8csW2n1xQXgJuVzDQDeNQIMzsYwdyC1lpkLly4hSrBabRHX9jUa5Agwb7lcc7jFtV2TPSxTfTlflfk6oc2nlkhp5RsSZt6Nf644bkOFVcsErVWqBVY0+SlVVwHo1Ry+bSA6RMH2sdUo8YrkCaVYgwGUqm0GQeQe74459xfM1Hy580VKQ85LswXdatoFGnMzt6dOsvRvmhfVx2TZBR4kzoy5c1pHXyixDA9SF0TA6gdvXC9XrTnWPMpiDIgiEANsEaTBlFNSzEMArKFLED2hO+xkSbQOwOAudy1UVmdFdza/tMYABmJPxx2TuUXFHKmoyjN9/wCMcMxXH/hhe9Rhff8AaUMUuBoVqv3CVvUSKdXtt/fwC5XipZ8sjTqWtzAgiJemf+UiPTF7g2fp+axJ5TTq3FiZpvAtseg+WOWGJxVF8+ZSkmmFFCyPMgc3R2P41iwPeB/KMUONqPJqaOhQadTN+MAEgyZ6dzftibVXcGpSyeYqoCZZKdQ0xBvzabERiuiZqstSnRoNqmSTUWUFu4UEW9oHcemKtb2c8LexS4TqWjmqZpqRoLS4M21C145WlgIsfiMLdKiW1ERyrqPuED5zA+OG+v4Nz7IFOXsDOo1KRMknoG7mcDuL8GrZOkFqgK1ZiRBDAqoIIPYyww1oeUGuEypRg0abX5GdDteYZRcHc6hGPVOsNYgCL3Y7jTsQLW9wwZ8D8NqEOxGyirTBIEtpqop9BMm++nAnOcDzFIkFJKSGCENpIsQwFwRF+ojEYrVJpdvzuVlPTBN9/wAbFzI57yqq1BDEBgVEwQVYEWjoekG2J+NoSCCSIYVNhAPYBTsQw3mIHfEn6OcuKmaPmyV8p/cCwVfdsx+GKOX4c/l+WtQmpUEeWyldwbajyg/G+OiCpUcs23vYQ4FUgP8A7xzf/wDGr9r9fd9MEPB2R86kkKZACggDdywFyP4Ysevrhbo061J6qGFZZDgi4LIVMdZ0k+nyw9fozqUVUq26tTOqy3mow6XjURHXEssLVIvhyqMtwbUzqPl6lJwec0yR5bXC6zusXBjrgbQqmiq+U7JTaQ+kwOU7NqLAQWPX8Q9MV+C8Mr12KrpBAjQQSQPakkLAE2BJuZ64Y6HhDMU6KSyF0ZurEBHILWVSZBA77GAcNHBaqzS9S7ugJ4gL0s9UenOgqokFoINJCwMNtN72sLWwQ4BxMvmA0IrImtVDeWKkgAAl2aBzap3hbY88HrNUrP5dagxC6KYIaQ40yRTZNTaUDXIIBg+mGujnXAFaqFq1KSlIoP5JcSW5oCnVpCp6kk9MXSemkczac9T23C751GoK0inUGoOBXBWAYVtelgCwg6bb45jxritSuzhyWIDIvUwpIGwuetu+OieJOK5dqbU6LvdAC1Su66WYez94dLNFyFPQX5hKw9SmU5MzQZjsrTrvYiQIsbDuI2nHPDG1K2jpzTjKFJlern2fM0gzQNAA7khB8ZhNM9rYv5bhFJGp0lBVWLBjMtDTN2nufngJm819/SrKyM1OrAAoN5are7BrHmG8jfa1mOlWeoqVqZlCwIYR+8V633tiqVsgp0uQL4qyVOlVCUwY0zcz0xF4blXNRSoNOlUYEiw+7YdtxIj1jFnjfDc1XqsyaQoGg6glyN4aQSLxvuDiGhkqlGmabZYVKzSPujdqRFPlIm5lY5RMVJvbCSg7dFITW1jBVy1V6f31cAE1HdabqoYkguDKE7jYR8sazWQGl0UaVAlauqb/AIQrKApIuSY/Ewtvhbq8UILE0HVnYMzWG+rSNLoQBHUAMxkyZJwMbP6logh1qSzhieVlhwSoB6MCJvdPeMSayXeyooskG1FKz1lqlU1Cq1KsamUMFSCQTNifQW6+mI0ovUqFS7hxeXVVMyBEBjBvv6Ye8lwhERczWINF0FXVT9ssQBtNkc3iBIYg2thcza0CZOaDMhIaaZ0qQLqAFCwSAbAE29Qb7kXxyUcvlcyaTlC8AENqgyoFRiFgwPYHxYd8VPMqAhitQKFi4AA6XJJEegwyVGq+XY1QWXUpXkKoomFViGGu1hvoWPawPocaFSk9OM2wJOoFKbkC4ImxEG4O9txGEcy2PH7O/LJsipYVGT2hSBMLBId0UdP4we+Icvwaq6u5zCqEJDhh7I7khh3+Y+GLnhvO0KaV9NPMLSFMQ1VQAZq0ZBcGGNyQIuMFqrZYr5nnmOUxBLMpBNtCsYnvAMHvOLYmu5zZdXC57itU4CwJBzlL4tH88aw6acuPaahO51UqLGTe503xvFNWPyJ08nj7lYotOJyjU9VpC6QfmDtPe04r1OGahUIjnPsM+kbRfmiTbb164D/rCw+/Qj8ILSRcC5077SbfSMWcjmKRcitVhOj01V7+oJBj6+mOjS/Jxxku6d/AH8U4W6ktTqIjMAGVKsAxN2sTPxxE+QBNxT6381e/bV8cEG43w+mZZM2w2nVQC9Oi6/zxmX8W8PUkrlsw7fh8xlbTvNlgXB6g/DEm8aHUcku33/0DM5wcRIqUr9BU1kXkzDdfniDLoFZlVwWP4QKk/K/5YKDxFkmcAU3pRP7Q+ZqNgF0rJB3Mnt8MW6VaiwVyEmDFtMAtVtptFlXp+Id8GOh7wW5OcZx9mb2Ktbg+YqIQtI321HT1nZgMDcz4ZzCzrFJZUjmq0/pJJ+QwxpXo6iIp2IB9bjYzewO3cY1U8rTq8tSQbmJsFTp/i1H4+mDKKlyjQlo4PWc8R5g6UprTpBVhvKIAY9T0HoBeLnFvhuQXMoWFSjzBDz+TqBhjfVRqCbn+UTBF/rFndadKkwPfSVUA7EmNMfHE2W4flFVda06VR0UlXdibC08wGr5nEpYtTs7MfqHCDjxdMY6dFgVnM0oB70ukf/bj+W2Br8JyyUfKaspLiWjyKYjmH4UUxBI26YVM1Wy7tNKgh5yTUZ6p5hBBAWpK7n2iOnbDEvidEMPSoHmJ1aQWYEswIBBgwVIEmNAHfE1hjH9KoZ55P9Tv4gQ5JqLnyFy5gys11M8kzzVdw0DpacEswaQJJpXYXIroDzXO1QdfnbfFrM+JkB/+QFpFqSn3W0Da/XqcUl46YKnKJLMdGuiFMk7XBgTAF7aF+Jio3d/cDyz012+Bup4NJqKfJNHS4JZmkm8wAGYEk9SQZ+WAHhujTTNM+Y1ilTeF5HYHnb90E2jcYP8AjTxNmKVfy6lArUCq0a/MiZi8sAfdi7w79HdF6aZqpXmo9PzFp6VEsVkTJktq3Ij4YL3ewE0uRqynixqiE0QzkxoHkukWQxE6z+Iao0yLlQLjczxviqhqyZZg4JIUIGmwEFvata3qd98K+f8AD1WjR8zMZmvU576S7NdSANIrbA31bzA2xUbgFJ0p1Gp1AKklGqsRZHAMqzloJEbbMfTE+nvwWjnTWzsN5vP52pT0VqFVS0+YqxT3BEaiIvA77i94wrVvEdBkakVby2gMrV3ggRExl5tA674tVeCZZSRyTeQCJEljtoJgaoH+FcXOHcFyvJTFNahNyRLEJpAkgU1Px7t64dYmlsgPPb3Ye4fw0+UK1NTFbKpJVyAWDVQsGUYQum8DbvOKXEOBe22kzUcnzFqVS2nUCRoLNEixtcEjqMX8lxqq9CnTXL6AKQVBS1VCF2EwgKxc7+/bFLN0TUI85ypHMVZKki0XtYb36zjmwJ65Sa5f4o6M0Y6VFdv+gmlUzFGqAabHkVQ8IxkAFiVqbkMJnfrjK3h1n0kUH5ruVZoJ11FOwsdJsZiIi2DWaSohBQ1HciJVGAUbySUEA+zv17TgxU4tVopSaqoQdm5YG11bmGLynpe0b+hGOKMk7lRzvjvA69Gn5ppuqcmtmsDyi8yD3HeVAxb4PnTTFeuCxf8AE2sqwkRymZBA6z1PezNx/P0swpAJqkgFaK81NxqWWK2ssTqFgGJucc3pZt0FRAsgk+ztAaIIFxGw9AMMmuWSlGtkda8GcMOXdiVYVHRSztV1EhjqUfiNgZkgG/piWtnaFPIkVrpV80FEb9pLNqEgCCRuSwud8KOW8WUF1Uiyl9RE+Wzk3a4KnYAJA6R78Wc/xuuGFRDUIEhSrU0CASRIYazHcRPfAtPuHdBdeFZHL0vMp00FiQQF1DuNb6qkjaR2wvcLyVIo5LlQ0vIcqCliBJIgw2ke7HvJcUrVKVRvKosSXYeaZBsWbZjtYAWjthdy/EHZjUWnSBJgwYC6g0L7MCQDHuw9eCbfkO1uCUCTpd/aIB+0A2kjck3gbx+JbWIwcTw9RRRUZqxhQQA5jVYiY0giPTod9sAs5xxaTaqTE6lswYMAwnXYaSQSTF9otjdTjtIqp84+Yd9dEimJ3OoMTA32/phtvIntN8AjhlUEim9XQhIE6SwTW9YMxAN4Eeu3wbxopvK1A6pJ1zAKgWOk3W/r1GAlLh5qIUpZrKvcsopVBTYk7alcTpBvE374ir+H84vmHQ9VWXSYVKoIOm4UTEH42OFgnHeg5KdKxlzebfLwlVGUspeG7M7t0MdT8sLvEuIGqyGizAqCNiAZM76wCOlx13xFxPibeT9meq7UwwJWFB1yd5Gux6HsBgFSylI8/NYaoZFWwJmJbf8ArjRmtO6Hljlq2kw5mQatJvMzFXzIBFEUmCu4DQupWgxLXKgQx361zw8hiv3roohWalUJAPNC7wNRMgxee84LeGfDdGqtQrmSojSLJSBLKw5hDO4Ejr1IkYzPeFsrRmc0pI9hVpKYgGZk3MXv12jbBjj1PdbAbkt73Kn60dkp0dZ0aQoB9kKASARpCiBAg/ujrvSr0alVKqVKtKitMlxqlfNBUmEMRqsRBi7DF/KJkKY5z5pImGsR8AFj4k74qq1M10XLqyS24OkhTqkDuNJixMzBsSMDJFpUCDt+Rj8HV6NLLvVzSU2DQAtSqwCU4VQqlQ1jAgTpvEiMAfFXH8t5yplKVOnRamdR03LMzGdTiV3BkEfAxifxCKb8qjlMpTUMAnmKGbaRpkgntzAeg59mGOsagVItHb4fyxFadOxd61LnYcK9ZqaBfLgshDB0UgqW1JyldPYzHTcxOB/DEqkA+YruJinULFYgQSWaNybAdOxxQpZ1tCoXcKpJF5Cg6ZgGQO9rfljYzTLfzGkCQLXsPTvb1j5L39wrlIe6IoaV1eVqgaoWjvF+nfGYDLnKn77f5jjMPSBv5BlDKIwH3EW3JgdNiVjG6ho0nGqmmwLKG1EjsSBAPphv/V9MJ59R00sPMJpNWsNySsrBvMGOuAHHsu9NyEqLpaFUNUqal1AS0ajZTNyT37DHZscUYO92/qyXhmbSpqNKkiIrBSwSYmYi94AJgx9Ri4nCeGC0VO5JCk3iAZkKOZYiJkb74H5aTQ1NEF2XSqs7LK0yGJaotlAKiT+Jt7nHuh5dQuqaQVAJ1U1AK9tXnET177YPK4QXGns39QivD+F76HtB2Ub6Y+epYH8Qx7XLcNiPLY7GSiTfTA951LY35hjXC8mrHzIp6Lg8oN+gUyQbG59AO+Jf1OEuFWoOoYQT8Rt/dtobS6tL7C9+X9T3m0yCKKhpOo3UhKY2vb1sbYF0+J06nOiV6V9JOuVKHaQVIBhbgWv1JGCeY4mgZQuWJAmAmk6TMGajkXt0/wC3jN5ypUUL5ZAIkgtBUhjAJFQ3gAyP3o74m5yukvsUWOFW3v8AEG0c3TLjzUfy7+yVDE9OYALBMb/W2DnC2DmszUTUoqyrSMMzFSJJ5ATqC6Dfv0xQynCU8wVD2goecGx/EQDYnpGwx64lSCcjUl0/e6D96QRVpIhltYuStx2AiDhpqQsEluE8twmkTJyxppN9VWopgrDcgqEzMRIjfY3xby+Sy41lKalkfQOWpUIUvmFIhzpP7MfXuJHV/EYAhadT6JE/xSTipV4syU3c6QXOsS1SEIas0gkQYNSelk9wEZLTyUjuMGTo5Zqj0/swLU6iK85XL/iqlul48ogd9ImC0jF/JcPytPRUfLqoWkXZmytFRqDrpJK3DRMBZESZkAY5p/8AEJpu7xTdmcMSHDaiq6ZtUmDuZ6kxExhn4DW8xGOulNOig0RohZ2JAblF4UqL4yihtW/BS4nQbMcVqVHZmoI1MkKxJgU+UKpBEa0JPoT3wfORpurQcwwM2c2nVrsALQGAERYaRtgrkOG5RqGqs6ipriARdYBFoJ+OLJ4Vk2El5i3tCPiIg/HDJJWFwU6ewlVsm1JV0mrT2J1MFUahUI9oljJVF0zq5n6xNOn4crsnmGooXSCGZ+YwDcK1PVE9fdA3x0TJ8GydRlC7D9xgpIA21KNUekxhhyOVy+Xb7ulT8xxOtiAW092gsSO8H34WUtJoYU2cafwnm6iNVZV0nZqjKoYwI0F6Y1sdgBMm2LfhThVWi9U1aPl66LUgSBH3lrsiwAIG/fD1W8HUquYfM161Ws7NKoGC06ateFnU0Aeo+GLWcyVCjSbQirAZpWSSFEiahJbp3viTyvsXjgS5/crcU4bTZKS0BSR10GWCpKpUVjdRM6lHzm+2K+Q8PlWRle1NbASV1ACG1O3obqD0uMMzqirzEGxLSLRLHYnvG+AnGarhWVmEFtZkLGlXlV26gQQZ6x3xHGm1R05dMXqKHFaSrVTy3OsurtLtpgVpaACd6hNjN2iwEBZ8cZ06VLMAS+iAOhB6m5uBew3t1xf4pxAa7GGkX2suk2Hb39zhP41mFqsYenKVCQHMLq5heAZ/qBjrjBKNHDOdysl4Jxp/Kq0aUy/KGaDr1AIoAlYkkxFreuGLgng5YGoLdArK7ipNw0FAAPaCkWvYzha4M1epVWlqoVWKOKaAcgJHMzckkQpW83aMHuGZV8pTq1MxSyb0p0M6EakqaintCmumJZYmxPvwJvhIONJ3qGjJMcupRaY1Ezy0QoAvGkQRYR+IzfvgVX8VVKLeXVb7yAQKVNFDAtHMSrdiOht8cLtDjdFQICJUE8yV6q8swBAjYQCT/PEmT8SUFBDrTe5PM5ft1eSIMkx3wY+RJypVFm+P5j7aoNPKoXWpZlegFAIWVPOGYgzY9YPbAXjVEU3lMtoQKoJarTY+ZEklULWkWFutxJxNU4nTaooXy1RHDKiiAoBEbEXBufVttsA+J51XqVZi7rsTJjXE39enc4DVG12jWezteVAUoqqJpMg0EAmTovA9d974ytxNdIFXL6tSf7NtKwSdNgp7TEyZ6dfObqBQIZgQDcMwn09qLe7rgzwehUKIwVq0iYFSqDsLyGgiRsdpPfCOMuBseROnQqUaYqsEp02EssgEtAAg3PqSfj0jDZwlKdOnofzkKKedS9Mkgm8KSCIOwMmPTDTleDhl1NTKdwWSYkEkajqmO467YH5vJoCVqZciQdJLG41WICgXj8+sThYVdLn4MvOMkk2tgZUor5a1FelU0tpJrPTW5gmC8QdMmPpgSmtnUA5cGbBalBmJIAAWDvqwX4sgpZcozeWlZ5urWJSCqwCegHUWPuwpcNydNa1MirqiohjQ4nmHUi2EWpN3+5pSjJBDLZia6sWNXSWaILqLQN7GCRsN4v2KJnx5gqOmhgpuHdABB/AT9cKnD2B8xQRGiJYWP3lPcCe2L3Dsm7qtNair7Z5WlSdCQp0ncwfn64fqbCKPtBxOEKQKmp01ixhYggG+nS03vc9MNuR4P5dK1ZWJTUmqkVIV1BF5aYufQxtfFbi/AatPJ0YUecppo6LpfSBSJJa8zK7gzf3RazzmgqHWAWosy8lZhoVtKSVe8bGbwVJ2OOfLmpJPyd2HBF3P3fcqN4VBAUmiYNyVqM8W2NoImxBwr/pF4AlA0qqvqNQsG6SV080G4JJMxbrbDXmuLVBUAWWRmRZisp5qpRpBf8KwJ6m+xAwB8R12rZFqlamNdPymRjrkF2cOOZjaAB8PQYSGZN6UPlxxUG/5yK/BsvTqHS7MpgsuhZlgQIMkADrJ9Opxri+TamAQQUadLDY7iD01D4z8MRcNdUfUxGiNJPNpJibxfpIHWMNfCeMJXb7MaZNNgANUAav8IPLHQgyD1vjtUFJe88uUmndbAhc6Ox+WMwxN4So/iqPMD/b5c9LfTGYbQ/AllLK8QqMlVHZX8ymyCLaCRE3Y6h/S2Js3T86r5pAB06eUg2gjfTsZ2wOpZfMCBUEk/u02aOtzAHynfElPJ5hmjy0P+JWUfEsABjo2rv8AQg3K+31DdGqiqFOXptGxYvI90GAPQCBjb1KRk/ZaM99VQ/8AMPzwv1PPX/YVOs6dXc9h8fjj1Tr1v/Jrj0ip/O2B8/sD4x+5PUpVQ+pG0XmEWFP+IXJ+eDGR420xVUj+JQxB94iR9fhgLTzlS3JWHT9nIHvOk4vUKjOIJYXi9M3+YAj3YMbT2aNKSrdMt5TT5lVQRp1agehDCfznGkz6anUggKYUgE6xG8QDvI+G+BH2s9KFVoEwwbe1og/n0xC+erXinoEE+zPQ9pI/0w/UXZk6l4GRM+I5UY+pgf64o5ziwYnU6CBsOb59Leo64AUBUqOfMNUqAxJ0kQI6Frf31xK+XpCdMT+9VM++06fpgPLa2QyhLuy2OJIR90pdp3N4+Vgd+3XG6Yq1pEiZBgwwtMyJA+GCnDeLrlqYAFP7wawTWPMBaY8uAtunzxJS8TsQfvaM79x/7DAv6/1VtPl/IaMWnaLHCPCqtJd8tTnpyE7dFVre4nvvgt9jydGFSnUzFWLadSIT6QY+EtgUnilgJWrSYxEFYA+IpmYPpf0xWzfimuV0tVQE9MvTIqH/AIiJHS+kD1wjaT2RZLyw1n6ChFastLLDqPaaTN43b+eKVfi1CkD5dIWJmpWhQT6UxF+vNHTAWmKzzVhaaEEmpUOpjAJPNeTAMqus2mMF8t4dC/evG96lbUogOJCL7ZtqiNI9nlg4DvuHWl+lfMGVM5Wr1DUUMzAAa2lEUdIpiI7c8T+9joPA8+75dWfRqQtzCYCSQSZFjI7kWJmMKhz9P9nQpHMVAoMwNCEcwOiyU95lo2FrYM8FasSBXdSxDoy05KgadRBJtaAOUDciemFSXYyk7thqnmZ0iWqEpBPspF5PoTj1UzVNlIZpg2VabMhAKmCRY3k79sAszmJbSTrAHsA6aaj+Ju3p8iNsTO6nK16y6TUSnVCsEHKQkjSCLwfS579UnjtHRjy6ZWG/No1WJAILcqinSjuTLEQOa8m0i+FbxLxInMeQkB2Ipos9Seh6DrMTA9Ma8OcVQZU+e019LqGdZcgg6eaL3tM9sBuMZ5k4g2Yo0RWAcsCLappaIJImAYNgbgnc4GPE4O6DlzKapuho4R4ZrrTJqVKHmNcKKWtVN4liwLH5bCMV2y70/Mq8TNCNJSmtJTJM2YLECBvMxO46rlfxNm69VQtNqUmNKQyt7yYI+A64KVkzOivNNyzoNEDVoINwGPeJ+MXxV6+yOf8AtmUOK5cc1MMukxy0p0iY9rWJhTPx72wZ8N5T7XTc0iNKtzpUQ0+YyZkawx6zHXvhXSlmadOHpZkk6/YcRBKtcTv0B6XHfDl4azJp5Zf2dN2FQv5gJctqOjUQLgDp2iMH264FqF87Ema8K5iVCPRQmbFiZ22Bpdu3fFLOeGM5So1mFak7rTYooUFi9iN0APQCdtUmYglOJeLqdLy7+yGDAUnuSLaGKwBPr1wtZrxPVzjCgjnKoyFmq1JEkEALqVlKsYDXNwAOsYXTLuNrjwhU4Hms+KtbLtTLVgGqaHhwNRUD8YKqIldJjmm4IwD494czS+ZXekBzl30mYBJJMSTAJjr6+jtx3MtlkqvSqitWYKPNpEOxlQoCjUSFRUXV0JZcK9TilaqHVjWpqVAYNqAJMTPNJW2xwji7obUuRazOasb3i0Y6ZwCsvlJTUqn/AIc1CTpS/lajNQghVJFyQQN4xzfNcFrFjpRnWJlbgehPQgRY3uO84auD8QegVdlYaaRQkaeWaZAMtKE29dsFrVdoXaNaR3zOX8o1HFcsRFP9spAUVMwlwqAr+zF5N9S/gkyLw5MwiebWdQo1wHAN2gkgoTEL372wm+JvE1aooNKo4XVJ5VtLVCOZbj9pF+ijrMyVMxmKdA1aPEZCSWQ66ZtBIW86vhHrgKMXwNKb7lrxc/kpTp0atZYgaqdQqzAKNMsIkXPY7YVqmUR6yVNVUnUpd676mkR1VCWAAAAP0xa8WZ6q1OlUq+23MwY6WhlBEAneCJ/1wEyfGAILBgJiYkSIm/pI+eCum3TE/uVcSGnwiqus6CQV/BzRzr26/wB9MRVMk5RCq1DcgSjTPLEQCPrgxlOLjWzFjqAMEAFbldI03Cxe8HcXHVjy/F9b0UJeVZKiMSC0KwlTACxMGd7euMscXwM5yXPIf8I5HzciWqFzWWoka2eSSQpVhJiSxmQ25sdiL8Y5/MZd8tlzWCkZZWZgaaISTWAgNTMGIFgARqECcH+A8QE1hUI5q1MzfcssXCxuFsdxiDjQpV6jPVUuUooCQxQ21EAwB+9No3xKWL2jpWa8dd2COEJms15a0cwhY0PMIFSmxpglbOvlCLQukH8KiRpLEd40yudo5SoubUlalVdB82k9xrY+wimYuWIg6enUnVX7HV8zKqtJ/ZZo1yCLqeYEiRPvVTHet4k4nmMzlqjVoKhYJVWB1EqJVWgLAkRJkHcHcdOPNCdSV1ZzdGJBK3gCzGSoAiZsIvA3gfCWXw7w/l81K4pEwCDSDgS0WJNgIkmOUdcLrZcCnqDKRMBgIOpgJVgTq0hZIMQb+7B5M4poVSIg3gtG71DGmdvfvsdsHG6e5si2L1bMMpK/ayY6jKAg+4g3xmDWQCvTRzTSWUMYJW5Em02vjWOvS/P7nLq9wRo8e4exKh2Xe51gGOx64vUc1kmUuKqADo1SD8FJnHPaPFKUw1MhZvpMmL9JAnEg45kp2r9v2a9v997scbyzXdnTFp/4I6HQqZN1DCsgBEwaqqfiCZB9Djy+RybEzWU22NdYv6TpPy/PHPl4xkmnlq2BP7Jeg/3392xCvGMrEaH1k8o0cpHTU3myDG9jHrg9WT7sZtL/AAR0JuHZHY1Rvv5gIHusQvwxaTg+VFxUBPc1UZvmZOObjOrt5S/N/X+L+5xJk83lHKqVdXvIFLUotNj54Jv6DDOTjvZOOWMttKHjMZjI02KVK+lgJImbe8IQfhjKeayTCVzBMmPj/kxz3MZ6jqOinqWbFgVJ96h2AvPU/wAsS5fiGUgl0dWEWWnqF/U1l6emG1z5s2uH/lD0+YyYJnMNyjVpEkx/hCScCuL+JcuaVSlRVgzAp7IblIgk81vQE+8YWBmqZgiksG9ywn3jUflJx4r8T5gBTTTo1bc0gsLHb5g4LnPuyTcZbJUa8pmiWGkCBqVOWekmQP8AXEr00QSyoBG5RRMX206m26CPXFWlmWqsqUwFY7FjMezsYt/wgYv5XgylVeoxYVAWABvZgDLdLOBBDdbi2Nx7ydbl/gaZSow82kCp1atFLZgwC8qhpBGszeNN98MXBOHUiin7PodiVYOIRLlFIFgzXpkWPa2A32aadVUATUNxIPN5lyd2Nup92CoyWhaL1KlSECLppsVJ1OsEtM2MG0dtsViGzdXi4NVhk6TVa+sr5jrrZQGqXEkIoXU4F5gxjVbK6iXzFY1Kmgt5NJzFgXGqp7RBsOWBfAYeICygIopUxphKcLzfctvfZ0YhiCYcjA6pnmURYA3t6DrNyfUk4aOO+RJZUtkOebzFNAaaaVSKgWmkKph0gnuYBMmTdrjElHiC+Z7UxUq9YAlCNjvcxPqcJHD6zM2kRPVj8PlhgyFFV1M4lljTFxdqYvsT7f0wzikLDI2+D3xfiK07tLmJSmkKs9NTWgmdz0HUWxRy3F2q04rpp5jFPWQoHSACBPr78Cs1nSqqxH7STbpsevvjFarxQ6YQX7sf6fljbLe/kHVKS4GL7Rl+yfFiSfmb48A5a4gev3jf9WEc0qpJOoA9SCdR+Mbem2NDJVO6x1EmD77X+OE6y8Flifk6Hkc9SpcyEDpJYn8zi6eOn9/645rlso5PM1hexNvdaAfXpg2kRtYXj++vr/rLxmn2I5Ho72N68aP72/8AFjDxQH8R7e1/YnCrSoaj6nY9t/Q9v7viZqLrHMfTmM7e7D7Ca5Bt2pMdTF2Pq52xhSgTs4ns5jADzLSbx3Mk/HEpzbKAQzrP7rEb9++2FtGWSw6KeXt+0/znFxOF5ZmD87e9hG/u9b4V6/FnksXeT2sPkIGKr54M0kEnuYJjpfrAwr3WxeGTS94pjBxTjTZV0o5ZKppqo1FSxOp2LG+kjVBET0sIAxdoZH7ZQ8wlxJ0BWPtmdJJAAiXkbfhnrinksq7RreF9kBQCeo6iw+eD/h7OQrpdtUNLdAN1jt1BEXPuxBKpbnS25R22EJ2OWNREKkayERjzwCwEmL9Nu4tvFXPcfNcoCAhQafu19qBeZmTEm298EOO5HM0syxpVgIM3LGZ5ryDa4EbWwq5Kg3mlWImmxJIkyU1HrvJXe2/wwrilJUhFumm+C/4qztSvUAqgJUCEtJtqE2E2Bm3yxPw/O5VHCeVVq0ypXQRSYgyDKkKT3N++K/Akp1c0xrqzUwtSoFViDImL+8YprmFSpTrJSVFksArOWKywglmN4BuPT4Fc2GtqLedpUPPLL5tKlYBWQlhtcEGYkT0MWwURKXmALWVQDAfSWVgB+IMIAJEzMWG1sUaOeSoZdNQnlDTKj0YMD2+uI81n6Y9pJW1gLwbtzTPaPjh6S27EJJyp9xw4TlkUo/nI+uoDACyxRh7Mkm03gbG9sR55lIzXrTFjMH7sAdh/2xK3k5elUNOnUhSjQ1UNdm0CCadh3HUYEZvigeFFEQQZJqmWVQbGEj3QMPk0iw1IaMnwZc3mHGooqujNzMGMhwNJg81tjuJHWxXjvgVXy7UaFSCetSTqMgi+wJIUbRbHP6WerFqhWo1IOQdNN+0xLFQSQZIPri2eIZkW+1Zgje9epv8APE9UUqofTNvVYmcT4NUoK61UKlagQ82xIJgr35Tfb32IpLm2A02IOm8SYERfe0D+ycM3GzVLF6tQ1ViWFRixMWiTfvF7W7YWM7lxTqMu4G3ugH5wR8RiEopbo6oyvZlynxOqoCq3KNtsaxRVz/ZxvG1PybSvB//Z",
        location: "UK",
        urlImage:
            "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Palace_of_Westminster_from_the_dome_on_Methodist_Central_Hall_%28cropped%29.jpg/1000px-Palace_of_Westminster_from_the_dome_on_Methodist_Central_Hall_%28cropped%29.jpg",
        desc:
            "London is the capital city of the United Kingdom. It is the U.K.'s largest metropolis and its economic, transportation, and cultural centre. London is also among the oldest of the world's great cities, with its history spanning nearly two millennia"));
    _places.add(PlaceModel(
        name: "Amman",
        location: "Jordan",
        urlImage:
            "https://upload.wikimedia.org/wikipedia/commons/1/10/Jamal_Abdul_Nasser_Circle_Amman_Jordan-1.jpg",
        desc:
            "Amman is the capital and the largest city of Jordan, and the country's economic, political, and cultural center. With a population of 4,061,150 as of 2021"));
    _places.add(PlaceModel(
        name: "Paris",
        location: "France",
        urlImage:
            "https://media.tacdn.com/media/attractions-splice-spp-674x446/06/fb/55/c8.jpg",
        desc:
            "Paris is located in northern central France, in a north-bending arc of the river Seine whose crest includes two islands, the Île Saint-Louis and the larger "));
    _places.add(PlaceModel(
        name: "Paris",
        location: "France",
        urlImage:
            "https://media.tacdn.com/media/attractions-splice-spp-674x446/06/fb/55/c8.jpg",
        desc:
            "Paris is located in northern central France, in a north-bending arc of the river Seine whose crest includes two islands, the Île Saint-Louis and the larger "));
  }
}
// base64 Done
// pub.dev plugins - cachedNetworkImage Done
// listview.builder horizental Done
// gridview.builder Done
