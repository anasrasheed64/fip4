import 'package:flutter/material.dart';

void main() {
  runApp(const AppScreen());
}

// stl stf
class AppScreen extends StatelessWidget {
  const AppScreen({Key? key}) : super(key: key); // constructor

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "batata",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        cardColor: Colors.grey[600],
        scaffoldBackgroundColor: Colors.white,
      ),
      // theme: ThemeData(
      //     primaryColor: Colors.orange,
      //     scaffoldBackgroundColor: Colors.white,
      //     colorScheme:
      //         ColorScheme.fromSwatch().copyWith(secondary: Colors.white),
      //     backgroundColor: Colors.red,
      //     splashColor: Colors.black),
      home: const ProfileScreen(),
    );
  }
}

class StackWidgetScreen extends StatelessWidget {
  const StackWidgetScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: [
            Image.asset("assets/fruit2.jpg",
                fit: BoxFit.fitHeight,
                height: MediaQuery.of(context).size.height),
            const Align(
              alignment: Alignment.center,
              child: Icon(
                Icons.ac_unit,
                color: Colors.red,
                size: 50,
              ),
            ),
            const Align(
              alignment: Alignment.topRight,
              child: Icon(
                Icons.ac_unit,
                color: Colors.blue,
                size: 50,
              ),
            ),
            Positioned(
              bottom: MediaQuery.of(context).size.height * 0.15,
              left: 0,
              right: 0,
              child: const Icon(
                Icons.ac_unit,
                color: Colors.green,
                size: 50,
              ),
            ),
            Positioned(
              bottom: MediaQuery.of(context).size.height * 0.13,
              left: 0,
              right: 0,
              child: const Icon(
                Icons.ac_unit,
                color: Colors.blue,
                size: 50,
              ),
            )
            // Positioned(child: Icon(Icons.ac_unit,color: Colors.red,size: 50,),right: 0,left: 0,bottom: 0,top: 0,)
            // Text(
            //   "Anas",
            //   style: TextStyle(color: Colors.black, fontSize: 32),
            // ),
          ],
        ),
      ),
    );
  }
}

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text("hello"),
          Stack(
            children: const [
              CircleAvatar(
                  backgroundImage: AssetImage("assets/fruit.jpg"), radius: 50),
              Positioned(
                bottom: 0,
                right: 0,
                child: Icon(Icons.camera),
              ),
            ],
          ),
          Container(
            height: 200,
            margin: const EdgeInsets.symmetric(horizontal: 10),
            child: Card(
              elevation: 4,
              shadowColor: Colors.black,
              color: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4),),
              child: const Center(child: Text("Hello")),
            ),
          ),
          Container(
            height: 200,
            margin: const EdgeInsets.symmetric(horizontal: 10),
            child: Card(
              elevation: 4,
              shadowColor: Colors.black,
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4),),
              child: const Center(child: Text("Hello")),
            ),
          ),
          Container(
            height: 200,
            margin: const EdgeInsets.symmetric(horizontal: 10),
            child: Card(
              elevation: 4,
              shadowColor: Colors.black,
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4),),
              child: const Center(child: Text("Hello")),
            ),
          )
          // ClipRRect( // network
          //   borderRadius: BorderRadius.circular(100),
          //   child: Image.asset(
          //     "assets/fruit.jpg",
          //     width: 150,
          //     height: 150,
          //   ),
          // )
        ],
      )),
    );
  }
}
