abstract class Shape {
  double area();

  double mo7e6();
}

class Rectangle extends Shape {
  double height;
  double weight;

  Rectangle(this.height, this.weight);

  @override
  double area() {
    return height * weight;
  }

  @override
  double mo7e6() {
    return 2 * height + 2 * weight;
  }

  @override
  String toString() {
    return 'Rectangle{height: $height, weight: $weight}';
  }
}

class Triangle extends Shape {
  double base;
  double height;

  Triangle(this.base, this.height);

  @override
  double area() {
    return 0.5 * base * height;
  }

  @override
  double mo7e6() {
    return base * height;
  }

  @override
  String toString() {
    return 'Triangle{base: $base, height: $height}';
  }
}

void main() {
  Rectangle rect = Rectangle(100, 70);
  Triangle triangle = Triangle(50, 70);
  // printRectangle(rect);
  // printTriangle(triangle);

  printShape(rect);
  printShape(triangle);
}

// void printRectangle(Rectangle rectangle) {
//   print("the Rectnangle area is " + rectangle.area().toString());
//   print("the Rectnangle mo7e6 is " + rectangle.mo7e6().toString());
//   print(rectangle.toString());
// }
//
// void printTriangle(Triangle triangle) {
//   print("the Triangle area is " + triangle.area().toString());
//   print("the Triangle mo7e6 is " + triangle.mo7e6().toString());
//   print(triangle.toString());
// }

void printShape(Shape shape) {
  print("the shape area is " + shape.area().toString());
  print("the shape mo7e6 is " + shape.mo7e6().toString());
  print(shape.toString());
}
