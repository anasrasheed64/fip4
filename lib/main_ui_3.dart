import 'package:flutter/material.dart';

void main() {
  runApp(const MyAppView());
}

// stl stf
class MyAppView extends StatelessWidget {
  // upper camel case
  const MyAppView({Key? key}) : super(key: key); // constructor
  // lower camel case
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "batata",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        cardColor: Colors.grey[600],
        scaffoldBackgroundColor: Colors.white,
      ),
      // theme: ThemeData(
      //     primaryColor: Colors.orange,
      //     scaffoldBackgroundColor: Colors.white,
      //     colorScheme:
      //         ColorScheme.fromSwatch().copyWith(secondary: Colors.white),
      //     backgroundColor: Colors.red,
      //     splashColor: Colors.black),
      home: ListScreen(),
    );
  }
}

// protected annotation  task
class Hello extends StatelessWidget {
  Hello({Key? key}) : super(key: key);
  Color cColor = Colors.red;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(children: [
          Expanded(
            flex: 2, // 2/6
            child: InkWell(
              onTap: () {
                // setState((){
                cColor = Colors.black;
                // });
              },
              onDoubleTap: () {
                print("onDoubleTap");
              },
              onLongPress: () {
                print("on long press");
              },
              child: Container(
                color: cColor,
                height: 100,
              ),
            ),
          ),
          Expanded(
            flex: 3, // 3/6
            child: Container(
              color: Colors.yellow,
              height: 100,
            ),
          ),
          Expanded(
            flex: 1, // 1/6
            child: Container(
              color: Colors.green,
              height: 100,
            ),
          ),
        ]),
      ),
    );
  }
}

class ListScreen extends StatefulWidget {
  const ListScreen({Key? key}) : super(key: key);

  @override
  State<ListScreen> createState() => _ListScreenState();
}

class _ListScreenState extends State<ListScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        // backgroundColor: Colors.red,
        body: Container(
          // height: 500,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Text("anas"),
                Container(
                  color: Colors.blueGrey,
                  height: 200,
                ),
                Container(
                  color: Colors.blueGrey,
                  height: 200,
                ),
                ListView(
                  children: [
                    Container(
                      color: Colors.red,
                      height: 200,
                      width: 50,
                    ),
                    Container(
                      color: Colors.green,
                      height: 200,
                      width: 50,
                    ),
                    Container(
                      color: Colors.orange,
                      height: 200,
                      width: 50,
                    ),
                    Container(
                      color: Colors.pink,
                      height: 200,
                      width: 50,
                    ),
                    Container(
                      color: Colors.cyan,
                      height: 200,
                      width: 50,
                    ),
                    Container(
                      color: Colors.grey,
                      height: 200,
                      width: 50,
                    ),
                  ],
                  physics: NeverScrollableScrollPhysics(),
                  padding: EdgeInsets.zero,
                  scrollDirection: Axis.vertical,
                  reverse: true,
                  shrinkWrap: true,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class ExpandedExampleScreen extends StatefulWidget {
  const ExpandedExampleScreen({Key? key}) : super(key: key);

  @override
  State<ExpandedExampleScreen> createState() => _ExpandedExampleScreenState();
}

class _ExpandedExampleScreenState extends State<ExpandedExampleScreen> {
  Color cColor = Colors.red;

  // state managament
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(children: [
          Expanded(
            flex: 2, // 2/6
            child: InkWell(
              onTap: () {
                cColor = Colors.black;
                setState(() {});
              },
              onDoubleTap: () {
                print("onDoubleTap");
              },
              onLongPress: () {
                print("on long press");
              },
              child: Container(
                color: cColor,
                height: 100,
              ),
            ),
          ),
          Expanded(
            flex: 3, // 3/6
            child: Container(
              color: Colors.yellow,
              height: 100,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "hello ",
                style: TextStyle(color: Colors.blue),
              ),
              Text(
                "anas",
                style: TextStyle(color: Colors.red),
              ),
            ],
          ),
          Text.rich(TextSpan(
              text: "Hello ",
              style: TextStyle(color: Colors.blue),
              children: [
                TextSpan(text: "Anas", style: TextStyle(color: Colors.red)),
                TextSpan(
                    text: "\nHi ",
                    style: TextStyle(color: Colors.pink),
                    children: [TextSpan(text: "Sae'd")]),
              ])),
          RotatedBox(child: Icon(Icons.accessibility_sharp), quarterTurns: 4),
          Expanded(
            flex: 1, // 1/6
            child: Container(
              color: Colors.green,
              height: 100,
            ),
          ),
        ]),
      ),
    );
  }
}
