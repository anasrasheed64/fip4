import 'package:fip4/task.dart';
import 'package:flutter/material.dart';

import 'input_widgets.dart';

void main() {
  runApp(const AppScreen());
}

// stl stf
class AppScreen extends StatelessWidget {
  const AppScreen({Key? key}) : super(key: key); // constructor

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "batata",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        cardColor: Colors.grey[600],
        scaffoldBackgroundColor: Colors.white,
      ),
      // theme: ThemeData(
      //     primaryColor: Colors.orange,
      //     scaffoldBackgroundColor: Colors.white,
      //     colorScheme:
      //         ColorScheme.fromSwatch().copyWith(secondary: Colors.white),
      //     backgroundColor: Colors.red,
      //     splashColor: Colors.black),
      home: const InputWidgets(),
    );
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

// final double size=10;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      //      override color
      floatingActionButton: FloatingActionButton(
          onPressed: () {},
          // splashColor: Colors.green,
          elevation: 8,
          tooltip: "add picture",
          child: const Icon(
            Icons.add_a_photo_outlined,
            color: Colors.red,
            size: 25,
            textDirection: TextDirection.rtl, // ctrl + space to view options
          )),

      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text(
              "asdasdfasdasdsldjhflsdhfjsdhfkjlashdfkjlashdfkjlhasdkjlfhsakjldfhkjasldhfkjlsahdfkjlashdfkjlsahdsfsdfsdfsdfsdfsdfdsdfkjlsdhfkjlsadhfkjlhf slkjdl",
              style: TextStyle(
                  fontSize: 15,
                  letterSpacing: 2,
                  wordSpacing: 10,
                  color: Colors.red,
                  // backgroundColor: Colors.grey[800],
                  fontWeight: FontWeight.normal,
                  locale: Locale("en"),
                  // decoration: TextDecoration.underline,
                  // decorationStyle: TextDecorationStyle.solid,
                  // decorationColor: Colors.green,
                  // decorationThickness: 2.5,
                  // backgroundColor: Colors.black.withOpacity(0.5),
                  // shadows: <Shadow>[
                  //   Shadow(
                  //     offset: Offset(10.0, 10.0),
                  //     blurRadius: 3.0,
                  //     color: Colors.black,
                  //   ),
                  //   // Shadow(
                  //   //   offset: Offset(30.0, -10),
                  //   //   blurRadius: 8.0,
                  //   //   color: Color.fromARGB(125, 0, 0, 255),
                  //   // ),
                  // ],
                  height: 2.5),
              textAlign: TextAlign.center,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: const [
                  Text(
                    "Hello",
                    style: TextStyle(fontSize: 20),
                  ),
                  Icon(Icons.rectangle),
                  Icon(Icons.rectangle),
                  Icon(Icons.rectangle),
                  Icon(Icons.rectangle),
                  Icon(Icons.rectangle),
                  Icon(Icons.rectangle),
                  Icon(Icons.rectangle),
                  Icon(Icons.rectangle),
                  Icon(Icons.rectangle),
                  Icon(Icons.rectangle),
                  Icon(Icons.rectangle),
                  Icon(Icons.rectangle),
                  Icon(Icons.rectangle),
                  Icon(Icons.rectangle),
                  Icon(Icons.rectangle),
                  Icon(Icons.rectangle),
                  Icon(Icons.rectangle),
                  Icon(Icons.rectangle),
                  Icon(Icons.rectangle),
                  Icon(Icons.rectangle),
                ],
              ),
            ),
            const Text(
              "asdasdfasdasdsldjhflsdhfjsdhfkjlashdfkjlashdfkjlhasdkjlfhsakjldfhkjasldhfkjlsahdfkjlashdfkjlsahdsfsdfsdfsdfsdfsdfdsdfkjlsdhfkjlsadhfkjlhf slkjdl",
              style: TextStyle(
                  fontSize: 15,
                  letterSpacing: 2,
                  wordSpacing: 10,
                  color: Colors.red,
                  // backgroundColor: Colors.grey[800],
                  fontWeight: FontWeight.normal,
                  locale: Locale("en"),
                  // decoration: TextDecoration.underline,
                  // decorationStyle: TextDecorationStyle.solid,
                  // decorationColor: Colors.green,
                  // decorationThickness: 2.5,
                  // backgroundColor: Colors.black.withOpacity(0.5),
                  // shadows: <Shadow>[
                  //   Shadow(
                  //     offset: Offset(10.0, 10.0),
                  //     blurRadius: 3.0,
                  //     color: Colors.black,
                  //   ),
                  //   // Shadow(
                  //   //   offset: Offset(30.0, -10),
                  //   //   blurRadius: 8.0,
                  //   //   color: Color.fromARGB(125, 0, 0, 255),
                  //   // ),
                  // ],
                  height: 2.5),
              textAlign: TextAlign.center,
            ),
            const Text(
              "asdasdfasdasdsldjhflsdhfjsdhfkjlashdfkjlashdfkjlhasdkjlfhsakjldfhkjasldhfkjlsahdfkjlashdfkjlsahdsfsdfsdfsdfsdfsdfdsdfkjlsdhfkjlsadhfkjlhf slkjdl",
              style: TextStyle(
                  fontSize: 15,
                  letterSpacing: 2,
                  wordSpacing: 10,
                  color: Colors.red,
                  // backgroundColor: Colors.grey[800],
                  fontWeight: FontWeight.normal,
                  locale: Locale("en"),
                  // decoration: TextDecoration.underline,
                  // decorationStyle: TextDecorationStyle.solid,
                  // decorationColor: Colors.green,
                  // decorationThickness: 2.5,
                  // backgroundColor: Colors.black.withOpacity(0.5),
                  // shadows: <Shadow>[
                  //   Shadow(
                  //     offset: Offset(10.0, 10.0),
                  //     blurRadius: 3.0,
                  //     color: Colors.black,
                  //   ),
                  //   // Shadow(
                  //   //   offset: Offset(30.0, -10),
                  //   //   blurRadius: 8.0,
                  //   //   color: Color.fromARGB(125, 0, 0, 255),
                  //   // ),
                  // ],
                  height: 2.5),
              textAlign: TextAlign.center,
            ),
            const Text(
              "asdasdfasdasdsldjhflsdhfjsdhfkjlashdfkjlashdfkjlhasdkjlfhsakjldfhkjasldhfkjlsahdfkjlashdfkjlsahdsfsdfsdfsdfsdfsdfdsdfkjlsdhfkjlsadhfkjlhf slkjdl",
              style: TextStyle(
                  fontSize: 15,
                  letterSpacing: 2,
                  wordSpacing: 10,
                  color: Colors.red,
                  // backgroundColor: Colors.grey[800],
                  fontWeight: FontWeight.normal,
                  locale: Locale("en"),
                  // decoration: TextDecoration.underline,
                  // decorationStyle: TextDecorationStyle.solid,
                  // decorationColor: Colors.green,
                  // decorationThickness: 2.5,
                  // backgroundColor: Colors.black.withOpacity(0.5),
                  // shadows: <Shadow>[
                  //   Shadow(
                  //     offset: Offset(10.0, 10.0),
                  //     blurRadius: 3.0,
                  //     color: Colors.black,
                  //   ),
                  //   // Shadow(
                  //   //   offset: Offset(30.0, -10),
                  //   //   blurRadius: 8.0,
                  //   //   color: Color.fromARGB(125, 0, 0, 255),
                  //   // ),
                  // ],
                  height: 2.5),
              textAlign: TextAlign.center,
            ),
            const Text(
              "asdasdfasdasdsldjhflsdhfjsdhfkjlashdfkjlashdfkjlhasdkjlfhsakjldfhkjasldhfkjlsahdfkjlashdfkjlsahdsfsdfsdfsdfsdfsdfdsdfkjlsdhfkjlsadhfkjlhf slkjdl",
              style: TextStyle(
                  fontSize: 15,
                  letterSpacing: 2,
                  wordSpacing: 10,
                  color: Colors.red,
                  // backgroundColor: Colors.grey[800],
                  fontWeight: FontWeight.normal,
                  locale: Locale("en"),
                  // decoration: TextDecoration.underline,
                  // decorationStyle: TextDecorationStyle.solid,
                  // decorationColor: Colors.green,
                  // decorationThickness: 2.5,
                  // backgroundColor: Colors.black.withOpacity(0.5),
                  // shadows: <Shadow>[
                  //   Shadow(
                  //     offset: Offset(10.0, 10.0),
                  //     blurRadius: 3.0,
                  //     color: Colors.black,
                  //   ),
                  //   // Shadow(
                  //   //   offset: Offset(30.0, -10),
                  //   //   blurRadius: 8.0,
                  //   //   color: Color.fromARGB(125, 0, 0, 255),
                  //   // ),
                  // ],
                  height: 2.5),
              textAlign: TextAlign.center,
            ),
            const Text(
              "asdasdfasdasdsldjhflsdhfjsdhfkjlashdfkjlashdfkjlhasdkjlfhsakjldfhkjasldhfkjlsahdfkjlashdfkjlsahdsfsdfsdfsdfsdfsdfdsdfkjlsdhfkjlsadhfkjlhf slkjdl",
              style: TextStyle(
                  fontSize: 15,
                  letterSpacing: 2,
                  wordSpacing: 10,
                  color: Colors.red,
                  // backgroundColor: Colors.grey[800],
                  fontWeight: FontWeight.normal,
                  locale: Locale("en"),
                  // decoration: TextDecoration.underline,
                  // decorationStyle: TextDecorationStyle.solid,
                  // decorationColor: Colors.green,
                  // decorationThickness: 2.5,
                  // backgroundColor: Colors.black.withOpacity(0.5),
                  // shadows: <Shadow>[
                  //   Shadow(
                  //     offset: Offset(10.0, 10.0),
                  //     blurRadius: 3.0,
                  //     color: Colors.black,
                  //   ),
                  //   // Shadow(
                  //   //   offset: Offset(30.0, -10),
                  //   //   blurRadius: 8.0,
                  //   //   color: Color.fromARGB(125, 0, 0, 255),
                  //   // ),
                  // ],
                  height: 2.5),
              textAlign: TextAlign.center,
            ),
            const Text(
              "asdasdfasdasdsldjhflsdhfjsdhfkjlashdfkjlashdfkjlhasdkjlfhsakjldfhkjasldhfkjlsahdfkjlashdfkjlsahdsfsdfsdfsdfsdfsdfdsdfkjlsdhfkjlsadhfkjlhf slkjdl",
              style: TextStyle(
                  fontSize: 15,
                  letterSpacing: 2,
                  wordSpacing: 10,
                  color: Colors.red,
                  // backgroundColor: Colors.grey[800],
                  fontWeight: FontWeight.normal,
                  locale: Locale("en"),
                  // decoration: TextDecoration.underline,
                  // decorationStyle: TextDecorationStyle.solid,
                  // decorationColor: Colors.green,
                  // decorationThickness: 2.5,
                  // backgroundColor: Colors.black.withOpacity(0.5),
                  // shadows: <Shadow>[
                  //   Shadow(
                  //     offset: Offset(10.0, 10.0),
                  //     blurRadius: 3.0,
                  //     color: Colors.black,
                  //   ),
                  //   // Shadow(
                  //   //   offset: Offset(30.0, -10),
                  //   //   blurRadius: 8.0,
                  //   //   color: Color.fromARGB(125, 0, 0, 255),
                  //   // ),
                  // ],
                  height: 2.5),
              textAlign: TextAlign.center,
            ),
            const Text(
              "asdasdfasdasdsldjhflsdhfjsdhfkjlashdfkjlashdfkjlhasdkjlfhsakjldfhkjasldhfkjlsahdfkjlashdfkjlsahdsfsdfsdfsdfsdfsdfdsdfkjlsdhfkjlsadhfkjlhf slkjdl",
              style: TextStyle(
                  fontSize: 15,
                  letterSpacing: 2,
                  wordSpacing: 10,
                  color: Colors.red,
                  // backgroundColor: Colors.grey[800],
                  fontWeight: FontWeight.normal,
                  locale: Locale("en"),
                  // decoration: TextDecoration.underline,
                  // decorationStyle: TextDecorationStyle.solid,
                  // decorationColor: Colors.green,
                  // decorationThickness: 2.5,
                  // backgroundColor: Colors.black.withOpacity(0.5),
                  // shadows: <Shadow>[
                  //   Shadow(
                  //     offset: Offset(10.0, 10.0),
                  //     blurRadius: 3.0,
                  //     color: Colors.black,
                  //   ),
                  //   // Shadow(
                  //   //   offset: Offset(30.0, -10),
                  //   //   blurRadius: 8.0,
                  //   //   color: Color.fromARGB(125, 0, 0, 255),
                  //   // ),
                  // ],
                  height: 2.5),
              textAlign: TextAlign.center,
            ),
            const Text(
              "asdasdfasdasdsldjhflsdhfjsdhfkjlashdfkjlashdfkjlhasdkjlfhsakjldfhkjasldhfkjlsahdfkjlashdfkjlsahdsfsdfsdfsdfsdfsdfdsdfkjlsdhfkjlsadhfkjlhf slkjdl",
              style: TextStyle(
                  fontSize: 15,
                  letterSpacing: 2,
                  wordSpacing: 10,
                  color: Colors.red,
                  // backgroundColor: Colors.grey[800],
                  fontWeight: FontWeight.normal,
                  locale: Locale("en"),
                  // decoration: TextDecoration.underline,
                  // decorationStyle: TextDecorationStyle.solid,
                  // decorationColor: Colors.green,
                  // decorationThickness: 2.5,
                  // backgroundColor: Colors.black.withOpacity(0.5),
                  // shadows: <Shadow>[
                  //   Shadow(
                  //     offset: Offset(10.0, 10.0),
                  //     blurRadius: 3.0,
                  //     color: Colors.black,
                  //   ),
                  //   // Shadow(
                  //   //   offset: Offset(30.0, -10),
                  //   //   blurRadius: 8.0,
                  //   //   color: Color.fromARGB(125, 0, 0, 255),
                  //   // ),
                  // ],
                  height: 2.5),
              textAlign: TextAlign.center,
            ),
            const Text(
              "asdasdfasdasdsldjhflsdhfjsdhfkjlashdfkjlashdfkjlhasdkjlfhsakjldfhkjasldhfkjlsahdfkjlashdfkjlsahdsfsdfsdfsdfsdfsdfdsdfkjlsdhfkjlsadhfkjlhf slkjdl",
              style: TextStyle(
                  fontSize: 15,
                  letterSpacing: 2,
                  wordSpacing: 10,
                  color: Colors.red,
                  // backgroundColor: Colors.grey[800],
                  fontWeight: FontWeight.normal,
                  locale: Locale("en"),
                  // decoration: TextDecoration.underline,
                  // decorationStyle: TextDecorationStyle.solid,
                  // decorationColor: Colors.green,
                  // decorationThickness: 2.5,
                  // backgroundColor: Colors.black.withOpacity(0.5),
                  // shadows: <Shadow>[
                  //   Shadow(
                  //     offset: Offset(10.0, 10.0),
                  //     blurRadius: 3.0,
                  //     color: Colors.black,
                  //   ),
                  //   // Shadow(
                  //   //   offset: Offset(30.0, -10),
                  //   //   blurRadius: 8.0,
                  //   //   color: Color.fromARGB(125, 0, 0, 255),
                  //   // ),
                  // ],
                  height: 2.5),
              textAlign: TextAlign.center,
            ),
            const Text(
              "asdasdfasdasdsldjhflsdhfjsdhfkjlashdfkjlashdfkjlhasdkjlfhsakjldfhkjasldhfkjlsahdfkjlashdfkjlsahdsfsdfsdfsdfsdfsdfdsdfkjlsdhfkjlsadhfkjlhf slkjdl",
              style: TextStyle(
                  fontSize: 15,
                  letterSpacing: 2,
                  wordSpacing: 10,
                  color: Colors.red,
                  // backgroundColor: Colors.grey[800],
                  fontWeight: FontWeight.normal,
                  locale: Locale("en"),
                  // decoration: TextDecoration.underline,
                  // decorationStyle: TextDecorationStyle.solid,
                  // decorationColor: Colors.green,
                  // decorationThickness: 2.5,
                  // backgroundColor: Colors.black.withOpacity(0.5),
                  // shadows: <Shadow>[
                  //   Shadow(
                  //     offset: Offset(10.0, 10.0),
                  //     blurRadius: 3.0,
                  //     color: Colors.black,
                  //   ),
                  //   // Shadow(
                  //   //   offset: Offset(30.0, -10),
                  //   //   blurRadius: 8.0,
                  //   //   color: Color.fromARGB(125, 0, 0, 255),
                  //   // ),
                  // ],
                  height: 2.5),
              textAlign: TextAlign.center,
            ),
            const Text(
              "asdasdfasdasdsldjhflsdhfjsdhfkjlashdfkjlashdfkjlhasdkjlfhsakjldfhkjasldhfkjlsahdfkjlashdfkjlsahdsfsdfsdfsdfsdfsdfdsdfkjlsdhfkjlsadhfkjlhf slkjdl",
              style: TextStyle(
                  fontSize: 15,
                  letterSpacing: 2,
                  wordSpacing: 10,
                  color: Colors.red,
                  // backgroundColor: Colors.grey[800],
                  fontWeight: FontWeight.normal,
                  locale: Locale("en"),
                  // decoration: TextDecoration.underline,
                  // decorationStyle: TextDecorationStyle.solid,
                  // decorationColor: Colors.green,
                  // decorationThickness: 2.5,
                  // backgroundColor: Colors.black.withOpacity(0.5),
                  // shadows: <Shadow>[
                  //   Shadow(
                  //     offset: Offset(10.0, 10.0),
                  //     blurRadius: 3.0,
                  //     color: Colors.black,
                  //   ),
                  //   // Shadow(
                  //   //   offset: Offset(30.0, -10),
                  //   //   blurRadius: 8.0,
                  //   //   color: Color.fromARGB(125, 0, 0, 255),
                  //   // ),
                  // ],
                  height: 2.5),
              textAlign: TextAlign.center,
            ),
            const Text(
              "asdasdfasdasdsldjhflsdhfjsdhfkjlashdfkjlashdfkjlhasdkjlfhsakjldfhkjasldhfkjlsahdfkjlashdfkjlsahdsfsdfsdfsdfsdfsdfdsdfkjlsdhfkjlsadhfkjlhf slkjdl",
              style: TextStyle(
                  fontSize: 15,
                  letterSpacing: 2,
                  wordSpacing: 10,
                  color: Colors.red,
                  // backgroundColor: Colors.grey[800],
                  fontWeight: FontWeight.normal,
                  locale: Locale("en"),
                  // decoration: TextDecoration.underline,
                  // decorationStyle: TextDecorationStyle.solid,
                  // decorationColor: Colors.green,
                  // decorationThickness: 2.5,
                  // backgroundColor: Colors.black.withOpacity(0.5),
                  // shadows: <Shadow>[
                  //   Shadow(
                  //     offset: Offset(10.0, 10.0),
                  //     blurRadius: 3.0,
                  //     color: Colors.black,
                  //   ),
                  //   // Shadow(
                  //   //   offset: Offset(30.0, -10),
                  //   //   blurRadius: 8.0,
                  //   //   color: Color.fromARGB(125, 0, 0, 255),
                  //   // ),
                  // ],
                  height: 2.5),
              textAlign: TextAlign.center,
            ),
            const Text(
              "asdasdfasdasdsldjhflsdhfjsdhfkjlashdfkjlashdfkjlhasdkjlfhsakjldfhkjasldhfkjlsahdfkjlashdfkjlsahdsfsdfsdfsdfsdfsdfdsdfkjlsdhfkjlsadhfkjlhf slkjdl",
              style: TextStyle(
                  fontSize: 15,
                  letterSpacing: 2,
                  wordSpacing: 10,
                  color: Colors.red,
                  // backgroundColor: Colors.grey[800],
                  fontWeight: FontWeight.normal,
                  locale: Locale("en"),
                  // decoration: TextDecoration.underline,
                  // decorationStyle: TextDecorationStyle.solid,
                  // decorationColor: Colors.green,
                  // decorationThickness: 2.5,
                  // backgroundColor: Colors.black.withOpacity(0.5),
                  // shadows: <Shadow>[
                  //   Shadow(
                  //     offset: Offset(10.0, 10.0),
                  //     blurRadius: 3.0,
                  //     color: Colors.black,
                  //   ),
                  //   // Shadow(
                  //   //   offset: Offset(30.0, -10),
                  //   //   blurRadius: 8.0,
                  //   //   color: Color.fromARGB(125, 0, 0, 255),
                  //   // ),
                  // ],
                  height: 2.5),
              textAlign: TextAlign.center,
            ),
            const Text(
              "asdasdfasdasdsldjhflsdhfjsdhfkjlashdfkjlashdfkjlhasdkjlfhsakjldfhkjasldhfkjlsahdfkjlashdfkjlsahdsfsdfsdfsdfsdfsdfdsdfkjlsdhfkjlsadhfkjlhf slkjdl",
              style: TextStyle(
                  fontSize: 15,
                  letterSpacing: 2,
                  wordSpacing: 10,
                  color: Colors.red,
                  // backgroundColor: Colors.grey[800],
                  fontWeight: FontWeight.normal,
                  locale: Locale("en"),
                  // decoration: TextDecoration.underline,
                  // decorationStyle: TextDecorationStyle.solid,
                  // decorationColor: Colors.green,
                  // decorationThickness: 2.5,
                  // backgroundColor: Colors.black.withOpacity(0.5),
                  // shadows: <Shadow>[
                  //   Shadow(
                  //     offset: Offset(10.0, 10.0),
                  //     blurRadius: 3.0,
                  //     color: Colors.black,
                  //   ),
                  //   // Shadow(
                  //   //   offset: Offset(30.0, -10),
                  //   //   blurRadius: 8.0,
                  //   //   color: Color.fromARGB(125, 0, 0, 255),
                  //   // ),
                  // ],
                  height: 2.5),
              textAlign: TextAlign.center,
            ),
            const Text(
              "asdasdfasdasdsldjhflsdhfjsdhfkjlashdfkjlashdfkjlhasdkjlfhsakjldfhkjasldhfkjlsahdfkjlashdfkjlsahdsfsdfsdfsdfsdfsdfdsdfkjlsdhfkjlsadhfkjlhf slkjdl",
              style: TextStyle(
                  fontSize: 15,
                  letterSpacing: 2,
                  wordSpacing: 10,
                  color: Colors.red,
                  // backgroundColor: Colors.grey[800],
                  fontWeight: FontWeight.normal,
                  locale: Locale("en"),
                  // decoration: TextDecoration.underline,
                  // decorationStyle: TextDecorationStyle.solid,
                  // decorationColor: Colors.green,
                  // decorationThickness: 2.5,
                  // backgroundColor: Colors.black.withOpacity(0.5),
                  // shadows: <Shadow>[
                  //   Shadow(
                  //     offset: Offset(10.0, 10.0),
                  //     blurRadius: 3.0,
                  //     color: Colors.black,
                  //   ),
                  //   // Shadow(
                  //   //   offset: Offset(30.0, -10),
                  //   //   blurRadius: 8.0,
                  //   //   color: Color.fromARGB(125, 0, 0, 255),
                  //   // ),
                  // ],
                  height: 2.5),
              textAlign: TextAlign.center,
            ),
            const FlutterLogo(
              size: 50,
            ),
            const SizedBox(height: 100),
            const SizedBox(child: Icon(Icons.add, size: 50)),
            Container(
                height: 100,
                width: 400,
                margin:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                padding: const EdgeInsets.all(30),
                decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: const BorderRadius.only(
                        topRight: Radius.circular(8),
                        bottomLeft: Radius.circular(8)),
                    border: Border.all(
                      color: Colors.black,
                      width: 4,
                      // strokeAlign: StrokeAlign.center,
                    )),
                alignment: Alignment.center,
                child: const Text(
                    "hellosldkjflsdhfkljshfkjlshfdgkjhsfdklgdfskjlhskjldfghkjlhkl")),
            // SizedBox(height: 20,),
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.9,
              height: MediaQuery.of(context).size.height * 0.2,
              child: ElevatedButton(
                onPressed: () {
                  print("hello Tala");
                },
                child: Text(
                  "Submit",
                  style: TextStyle(color: Colors.black),
                ),
                style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.red,
                    elevation: 20,
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(4),
                    foregroundColor: Colors.grey,
                    shadowColor: Colors.red,
                    side: BorderSide(color: Colors.black, width: 2)),
                onLongPress: () {
                  print("Hello reem");
                },
              ),
            ),
            Image.asset(
              "assets/fruit2.jpg",
              width: 400,
              fit: BoxFit.cover,
              height: 400,
            ),
            const SizedBox(height: 100),
          ],
        ),
      ),
      appBar: AppBar(
        title: const Text("FIP4",
            style: TextStyle(color: Colors.white, fontSize: 20)),
        elevation: 20,
        shadowColor: Colors.red,
        centerTitle: true,
        leading: Row(
          children: [
            Icon(Icons.remove),
            Icon(Icons.remove),
          ],
        ),
        actions: [
          Icon(Icons.abc),
          Icon(Icons.abc),
          Icon(Icons.abc),
        ],
        leadingWidth: 50,
        backgroundColor: Colors.red,
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(100),
          child: Container(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Icon(Icons.dangerous), Icon(Icons.abc)],
          )),
        ),
        bottomOpacity: 0.9,
        primary: true,
        toolbarHeight: 65,
        flexibleSpace: Container(
          height: MediaQuery.of(context).viewPadding.top,
          color: Colors.black,
          alignment: Alignment.bottomCenter,
        ),
      ),
      drawer: const Drawer(child: Center(child: Text("asd"))),
      endDrawer: const Drawer(
        width: 200,
      ),
      bottomNavigationBar: Container(
          width: 300,
          height: 50,
          color: Colors.red,
          child: const Center(
            child: Text("hello"),
          )),
    );
  }
}
