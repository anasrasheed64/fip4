void main() {
  // new object
  // print toString
  // add , commit , push , merge request

  Generic<int> object =
      Generic(name: "Anas", age: 30, value: 10); // a5s61das65d1

  Generic<int> object2 =
      Generic(name: "Anas", age: 30, value: 11); // asd894d89qe4d


  // print(object==55);
  print(object == 10); // false
  print(object == object2); // false
  print(object == Generic(name: "Anas", age: 30, value: 10)); // true
  print(object.toString());

}

// class generic String  name int age T value

class Generic<T> extends Object {
  String? name;
  int? age;
  T? value;

  Generic({this.name, this.age, this.value});

  T hello() {
    //
    if (value is int) {
      int k = value as int; // casting   int.tryparse   .toString
      k += 2;
      return value!;
    }
    return value!;
  }

  @override
  String toString() {
    return "Name = $name age = $age Value = $value";
  }

  @override
  bool operator ==(Object other) {
    if (other is Generic) {
      return (name == other.name && age == other.age && value == other.value);
    }
    return false;
  }

  @override
  int get hashCode {
    return super.hashCode;
  }
}
