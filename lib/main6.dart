void main() {
  // BankAccount account=BankAccount(); // =! new object from abstract
  // account.deposit(10);
  // BankAccount account = SavingAccount(); // polymorphism
  // account.deposit(10);
  // account.withdraw(20);
  //
  // BankAccount salaryAccount = SalaryAccount();
  // salaryAccount.withdraw(10);
  // salaryAccount.deposit(20);

  Rectangle rectangle = Rectangle(height: 10, width: 8);
  print(rectangle.area() + 30);
  print(rectangle.mo7e6() + 30);

  Square square = Square(height: 10);
  print(increase30(square.mo7e6()));
  print(increase30(square.area()));

  printShape(rectangle);
  printShape(square);
  printShape(square); // child
  printShape(rectangle); // dependency injection
}
//
// void printRectangleInfo(Rectangle rectangle) {
//   print("The Information about the rectangle is " + rectangle.mo7e6().toString()+rectangle.area().toString());
// }
//
// void printSquareInfo(Square square) {
//   print("The Information about the square is " + square.mo7e6().toString()+square.area().toString());
// }

void printShape(Shape shape) {
  // super class
  print("The Information about the shape is " +
      shape.area().toString() +
      shape.mo7e6().toString());
}

double increase30(double value) {
  return value + 30;
}

// abstract class BankAccount {
//   double balance;
//
//   void withdraw(double value);
//
//   void deposit(double value);
// }
//
// class SavingAccount extends BankAccount {
//   @override
//   void deposit(double value) {
//     balance += value + (value * 0.02);
//   }
//
//   @override
//   void withdraw(double value) {
//     balance += value + (value * 0.02);
//   }
// }
//
// class SalaryAccount extends BankAccount {
//   @override
//   void deposit(double value) {
//     balance += value;
//   }
//
//   @override
//   void withdraw(double value) {
//     balance += value - 0.25;
//   }
// }
//
abstract class Shape {
  double area();

  double mo7e6();
}

class Rectangle extends Shape {
  double? height;
  double? width;

  Rectangle({this.height, this.width}); // named constructor

  @override
  double area() {
    return height! * width!;
  }

  @override
  double mo7e6() {
    return (2 * width! + 2 * height!);
  }

  @override
  String toString() {
    return 'Rectangle{height: $height, width: $width}';
  }
}

// dependency injection
class Square extends Shape {
  double? height;

  Square({this.height});

  @override
  double area() {
    return height! * height!;
  }

  @override
  double mo7e6() {
    return 4 * height!;
  }

  @override
  String toString() {
    return 'Square{height: $height}';
  }
}
