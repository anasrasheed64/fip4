class PlaceModel {
  String name;
  String location;
  String urlImage;
  String? base64Img;
  String desc;

  PlaceModel(
      {required this.name,
      required this.location,
      required this.urlImage,
      this.base64Img,
      required this.desc});
}
