import 'package:fip4/main2.dart';
import 'package:fip4/main6.dart';

class MyObject<T, E> {
  // generics
  T _value;
  E? _value2;
  T? value3;
  String? name;
  int? age;

  MyObject(this._value);

  T get value => _value;

  set value(T value) {
    _value = value;
  }
}

void main() {
  print("Anas");
  MyObject<int, String> myObject = MyObject(20);
  myObject.value = 30;
  MyObject<String, String> myObject2 = MyObject("asd");
  MyObject<bool, Person> myObject3 = MyObject(true);
  MyObject<List<String>, List<int>> myObject4 = MyObject(["anas", "sami"]);
  MyObject<Person, Shape> myObject5 = MyObject(Person("asd", 20, 20, 5));
  MyObject myObject6 = MyObject(false);
  print('mohammed');
  print('mohammed');
  print('mohammed');
  print('mohammed');
  print('mohammed');
  print('mohammed');
  print('mohammed');
  print('mohammed');
  // List<int> data; //init
  // print(data.length);
  // data = [];
  // print(data.length); // todo fix this code

  onTab((String value) {
    print("x called" + value);
  });
}

void onTab(Function x) {
  print("onTabCalled");
  x.call("myValue");
}
