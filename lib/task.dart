import 'package:flutter/material.dart';

class TaskScreen extends StatefulWidget {
  const TaskScreen({Key? key}) : super(key: key);

  @override
  State<TaskScreen> createState() => _TaskScreenState();
}

class _TaskScreenState extends State<TaskScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: const [
              Icon(Icons.cloud),
              Icon(Icons.cloud),
            ],
          ),
          const Icon(Icons.cloud),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              Icon(Icons.cloud),
              Icon(Icons.cloud),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: const [
              Icon(Icons.cloud),
              Icon(Icons.cloud),
              Icon(Icons.cloud),
            ],
          ),
          Row(
            children: [
              const Icon(Icons.cloud),
              const Icon(Icons.cloud),
              Expanded(child: Container()),
              const Icon(Icons.cloud),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Icon(Icons.cloud),
              const Icon(Icons.cloud),
              Row(
                children: const [
                  Icon(Icons.cloud),
                  Icon(Icons.cloud),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}

class Task2Screen extends StatefulWidget {
  const Task2Screen({Key? key}) : super(key: key);

  @override
  State<Task2Screen> createState() => _Task2ScreenState();
}

class _Task2ScreenState extends State<Task2Screen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Icon(Icons.cloud),
                  Icon(Icons.cloud),
                ],
              ),
              const Align(
                  alignment: Alignment.centerRight, child: Icon(Icons.cloud)),
              const Align(
                  alignment: Alignment.centerLeft, child: Icon(Icons.cloud)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: const [
                  Icon(Icons.cloud),
                  Icon(Icons.cloud),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: const [
                  Icon(Icons.cloud),
                  Icon(Icons.cloud),
                  Icon(Icons.cloud),
                  Icon(Icons.cloud),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Icon(Icons.cloud),
                  Icon(Icons.cloud),
                ],
              ),
              // Container(child: Icon(Icons.cloud),alignment: Alignment.centerRight,),
              // Padding(child: Icon(Icons.cloud),padding: EdgeInsets.symmetric(horizontal: 100),)
            ]),
      ),
    );
  }
}

class Task3Screen extends StatelessWidget {
  const Task3Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: const Text("First Screen"),
        centerTitle: true,
        actions: const [
          Icon(
            Icons.alarm,
            color: Colors.red,
          ),
          SizedBox(
            width: 10,
          ),
        ],
        leading: Row(children: const [
          Icon(Icons.menu),
          SizedBox(
            width: 5,
          ),
          Icon(Icons.save),
        ]),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Icon(
              Icons.ac_unit,
              color: Colors.blue,
              size: 40,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: const [
                Icon(
                  Icons.ac_unit,
                  color: Colors.blue,
                  size: 40,
                ),
                Icon(
                  Icons.person,
                  color: Colors.red,
                  size: 40,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: const [
                Icon(
                  Icons.ac_unit,
                  color: Colors.blue,
                  size: 40,
                ),
                Icon(
                  Icons.person,
                  color: Colors.red,
                  size: 40,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const [
                Icon(
                  Icons.ac_unit,
                  color: Color.fromRGBO(252, 201, 79, 0.9),
                  size: 40,
                ),
                Icon(
                  Icons.person,
                  color: Colors.red,
                  size: 40,
                ),
                Icon(
                  Icons.ac_unit,
                  color: Colors.blue,
                  size: 40,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class Task4Screen extends StatelessWidget {
  const Task4Screen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.cyan,
        title: Icon(
          Icons.check,
          color: Colors.green.shade900,
        ),
        actions: const [
          Icon(
            Icons.alarm,
            color: Colors.black87,
          ),
          SizedBox(
            width: 10,
          )
        ],
        leading: Row(
          children: const [
            Text(
              "FIP4",
              style: TextStyle(color: Colors.red),
            ),
            SizedBox(
              width: 5,
            ),
            Text(
              "FIP4",
              style: TextStyle(color: Colors.red),
            )
          ],
        ),
        leadingWidth: 70,
      ),
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              Icon(
                Icons.ac_unit,
                size: 40,
              ),
              Icon(Icons.accessibility_sharp, size: 40)
            ],
          ),
          const Spacer(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              Icon(Icons.ac_unit, size: 40),
              Icon(Icons.accessibility_sharp, size: 40)
            ],
          )
        ],
      ),
    );
  }
}
