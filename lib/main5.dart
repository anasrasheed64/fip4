import 'package:fip4/main2.dart';

void main() {
  PartTimeEmployee kamal =
      PartTimeEmployee("Kamal", 22, 173, 82, 600, "2001516165", "256652", 1, 8);

  FullTimeEmployee mohammad = FullTimeEmployee(
      "Mohammad", 23, 170, 75, 600, "9996516516", "19651", 2, 8);

  print(kamal.toString());
  print("*********************");
  print(mohammad.toString());

  Employee anas = Employee(
      "Kamal", 22, 173, 82, 600, "2001516165", "256652", 1); // polymorphism
  print(anas.toString());
  // anas.hello();
  A a = A();
  print(a.toString()); // instance of A
}

class Employee {
  String name;
  double age;
  double height;
  double weight;
  double salary;
  String ssn;
  String employeeId; // lower camel case
  int yearsOfExp;

  Employee(this.name, this.age, this.height, this.weight, this.salary, this.ssn,
      this.employeeId, this.yearsOfExp);

  @override
  String toString() {
    return "name $name , age $age , height $height weight $weight salary $salary ssn $ssn employeeID $employeeId years of experience $yearsOfExp";
  }
}

class PartTimeEmployee extends Employee {
  // upper camel case
  double hourlyPaid;

  PartTimeEmployee(String name, double age, double height, double weight,
      double salary, String ssn, String empId, int years, this.hourlyPaid)
      : super(name, age, height, weight, salary, ssn, empId, years);

  @override
  String toString() {
    return "HourlyPaid $hourlyPaid" + super.toString();
  }
}

class FullTimeEmployee extends Employee {
  double monthlyPaid;

  FullTimeEmployee(String name, double age, double height, double weight,
      double salary, String ssn, String empId, int years, this.monthlyPaid)
      : super(name, age, height, weight, salary, ssn, empId, years);

  @override
  String toString() {
    return "monthlyPaid $monthlyPaid" + super.toString();
  }
}

class A extends Object{}
