import 'package:fip4/main2.dart';

void main() {
  String? name; // nullable
  name = "anas";
  print(name);
  // List<int>? values; // nullable
  //
  // print(values);
  //
  // print(values!.length);
  // print(values.indexOf(2));
  Person person = Person();
  person.name="Anas";
  person.age=30;
  print(person.age);
  person.height=25;
  print(person.height);
  print(person.data??_initData(person));
  print(person.data);
  print(person.data!.length);

}

_initData(Person person) {
  person.data=[];
  print("data list has been initalized");
}

class Person {
  late String name;
  late int age;
  late double height;
   List<int>? data;
}
// version control